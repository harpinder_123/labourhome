import store from '../redux/store';
export const Token = user =>
  store.dispatch({type: 'token', paylod: user ? user : ''});
export const Phone = payload => store.dispatch({type: 'phone', payload});

export const SetUser = payload => ({type: 'user', payload});
export const SetDeviceInfo = deviceInfo => ({
  type: 'SetDeviceInfo',
  payload: deviceInfo,
});
export const SetUserDetail = payload => ({type: 'setUserDetail', payload});
export const Language = payload => ({type: 'language', payload});
export const CustomerHomeJob = payload => ({
  type: 'customerHomeJob',
  payload,
});
export const SubscriptionPlanCustomer = payload => ({
  type: 'subscriptionPlan',
  payload,
});
export const activeSubscriptionPlanCustomer = payload => ({
  type: 'activeSubscription',
  payload,
});
export const VideoTutorialCustomer = payload => ({
  type: 'videoTutorial',
  payload,
});
