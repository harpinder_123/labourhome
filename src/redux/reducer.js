const data = {
  userDetail: {},
  user: {},
  phone: '',
  deviceInfo: {
    id: '',
    token: '',
    model: '',
    os: '',
  },
  customerHomeJob: '',
  subscriptionPlan: '',
  activeSubscriptionPlan: '',
  videoTutorial: '',

  language: 'en',
};
const reducer = (state = data, action) => {
  switch (action.type) {
    case 'setUserDetail':
      return {
        ...state,
        userDetail: action.payload,
        isLogin: true,
      };
    case 'language':
      return {...state, language: action.payload};
    case 'setDeviceInfo':
      return {
        ...state,
        deviceInfo: action.payload,
      };
    case 'phone':
      return {
        ...state,
        phone: action.payload,
      };
    case 'customerHomeJob':
      return {
        ...state,
        customerHomeJob: action.payload,
      };
    case 'subscriptionPlan':
      return {
        ...state,
        subscriptionPlan: action.payload,
      };
    case 'activeSubscriptionPlan':
      return {
        ...state,
        activeSubscriptionPlan: action.payload,
      };
    case 'videoTutorial':
      return {
        ...state,
        videoTutorial: action.payload,
      };

    default:
      return state;
  }
};
export default reducer;
