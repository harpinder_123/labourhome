import AsyncStorage from '@react-native-async-storage/async-storage';
import {request, requestGet, requestMultipart} from './ApiSauce';
import {Dimensions} from 'react-native';
const {width, height} = Dimensions.get('window');

const AspectRatio = () => width / height;

const Api = {
  LoginApi: json => request('/login', json),
  verfiyOtpApi: json => request('/verfiyOtp', json),
  ContractorRegisterApi: json => request('/contractorRegister', json),
  customerRegisterApi: json => request('/customerRegister', json),
  getTradeApi: json => requestGet('/getTrade', json),
  getProjectApi: json => requestGet('/getProject', json),
  GetWorkerApi: json => requestGet('/getWorker', json),

  HomepageCustomerApi: json => requestGet('/homepage_customer', json),
};
const LocalStorage = {
  setToken: token => AsyncStorage.setItem('authToken', token),
  getToken: () => AsyncStorage.getItem('authToken'),
  setLanguage: language => AsyncStorage.setItem('language', language),
  getLanguage: () => AsyncStorage.getItem('language'),
  setUserTypes: userType =>
    AsyncStorage.setItem('userType', JSON.stringify(userType)),
  getUserTypes: () => AsyncStorage.getItem('userType'),
  setFirstTimeOpen: () => AsyncStorage.setItem('firstTimeOpen', 'false'),
  getFirstTimeOpen: () => AsyncStorage.getItem('firstTimeOpen'),
  setSelectTrade: selectTrade =>
    AsyncStorage.setItem('selectTrade', selectTrade),
  getSelectTrade: () => AsyncStorage.getItem('selectTrade'),

  setSelectUserType: selectUserType =>
    AsyncStorage.setItem('selectUserType', selectUserType),
  getSelectUserType: () => AsyncStorage.getItem('selectUserType'),

  setSelectTrade: selectTrade =>
    AsyncStorage.setItem('selectTrade', selectTrade),
  getSelectTrade: () => AsyncStorage.getItem('selectTrade'),
  clear: AsyncStorage.clear,
};

//const onSearchEvent = new Subject().pipe(debounceTime(500));

export {width, height, Api, LocalStorage};
