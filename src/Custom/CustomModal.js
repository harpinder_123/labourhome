import React, { useState } from 'react';
import {
    Dimensions,
    Image,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

const { height, width } = Dimensions.get('window');

function CustomModal(navigation) {

    const {
        openVisible, openModal, title, description, buttonTitle, buttonfontSize, onclickPress,
        buttonWidth, buttonHeight, buttonBgColor, buttonRadius, modalImg, titleFontSize
    } = navigation;

    return (
        <Modal
            visible={openVisible}
            transparent={true}
            onRequestClose={() => {
                // Alert.alert("Modal has been closed.");
                openModal(false);
            }}>
            <View style={styles.modal_View}>
                <View style={styles.mdtop}>
                    <Image
                        style={{ width: 73, height: 73, resizeMode: 'contain', alignSelf: 'center', marginVertical: 20 }}
                        source={modalImg}
                    />
                    <Text style={[styles.titleText, {
                        fontSize: titleFontSize ? titleFontSize : 20,
                    }]}>{title}</Text>
                    {description &&
                        <Text style={styles.subtext}>{description}</Text>
                    }

                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={[styles.mdbottomView, {
                            width: buttonWidth, height: buttonHeight,
                            borderRadius: buttonRadius ? buttonRadius : 25,
                            backgroundColor: buttonBgColor ? buttonBgColor : '#6CBDFF'
                        }]}
                        onPress={() => { onclickPress() }}>
                        <Text style={[styles.mdBottomText, {
                            fontSize: buttonfontSize ? buttonfontSize : 16,
                        }]}>{buttonTitle}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal >
    );
};


const styles = StyleSheet.create({
    modal_View: {
        backgroundColor: '#000000aa',
        flex: 1,
    },
    mdtop: {
        backgroundColor: '#FFFFFF',
        marginTop: height / 3,
        marginHorizontal: 20,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleText: {
        fontFamily: 'Avenir-Heavy',
        fontWeight: 'bold',
        color: '#333333',
        textAlign: 'center',
    },
    subtext: {
        fontFamily: 'Avenir-Medium',
        fontWeight: '500',
        fontSize: 16,
        color: '#3333334d',
        textAlign: 'center',
        marginTop: 10,
    },
    mdbottomView: {
        marginVertical: 20,
        // marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mdBottomText: {
        fontFamily: 'Avenir-Heavy',
        fontWeight: '900',
        color: '#FFFFFF',
        textAlign: 'center',
    },
    topv4_Style: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        marginHorizontal: 20,
        justifyContent: 'space-between',
    },
    text2_Style: {
        fontFamily: 'Avenir-Heavy',
        fontSize: 20,
        color: '#9F9F9F',
        fontWeight: 'bold',
    },
    subtext2_Style: {
        fontFamily: 'Avenir-Heavy',
        fontSize: 14,
        color: '#9F9F9F',
        fontWeight: 'bold',
    },
    line: {
        borderColor: '#00000020',
        borderWidth: 0.5,
        marginTop: 15,
    },
});

export default CustomModal;
