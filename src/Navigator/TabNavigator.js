import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, StyleSheet} from 'react-native';
import Home from '../screens/Home';
import MyBooking from '../screens/MyBooking';
import MyJobs from '../screens/MyJobs';
import MyInquiry from '../screens/MyInquiry';
import MyAccount from '../screens/MyAccount';
import stringsoflanguages from '../language';

const iconPath = {
  h: require('../images/home1.png'),
  ha: require('../images/home.png'),
  s: require('../images/booking1.png'),
  sa: require('../images/booking.png'),
  f: require('../images/jobs1.png'),
  fa: require('../images/jobs.png'),
  p: require('../images/inquiry1.png'),
  pa: require('../images/inquiry.png'),
  a: require('../images/account1.png'),
  Aa: require('../images/account.png'),
};

const Tab = createBottomTabNavigator();
const TabIcon = source => <Image source={source} style={styles.tabIcon} />;

const TabNavigator = () => {
  const {_tabMain} = stringsoflanguages;
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        labelStyle: {
          paddingBottom: 2,
          fontSize: 10,
          fontFamily: 'Avenir-Roman',
          fontWeight: '400',
        },
        activeTintColor: '#6CBDFF',
        activeBackgroundColor: '#FFFFFF',
        inactiveBackgroundColor: '#FFFFFF',
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: _tabMain.Home,
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.h : iconPath.ha),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="MyBooking"
        component={MyBooking}
        options={{
          tabBarLabel: _tabMain.MyBooking,
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.s : iconPath.sa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="MyJobs"
        component={MyJobs}
        options={{
          tabBarLabel: _tabMain.MyJobs,
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.f : iconPath.fa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="MyInquiry"
        component={MyInquiry}
        options={{
          tabBarLabel: _tabMain.MyInquiry,
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.p : iconPath.pa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="MyAccount"
        component={MyAccount}
        options={{
          tabBarLabel: _tabMain.MyAccount,
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.a : iconPath.Aa),
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};
export default TabNavigator;

const styles = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});
