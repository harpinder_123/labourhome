import React from 'react';
import {View, Image, ScrollView, Dimensions} from 'react-native';
const {width} = Dimensions.get('window');
// const height = width * 0.3;

const images = [
  {
    source: require('../images/banner.png'),
  },
  {
    source: require('../images/banner.png'),
  },
  {
    source: require('../images/banner.png'),
  },
];

const banner = () => {
  return (
    <View style={{marginTop: 20}}>
      <ScrollView horizontal>
        {images.map((item, index) => {
          const {source} = item;
          return (
            <Image
              key={index}
              source={source}
              style={{
                width: width - 30,
                height: 150,
                marginHorizontal: 15,
                resizeMode: 'contain',
              }}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};
export default banner;
