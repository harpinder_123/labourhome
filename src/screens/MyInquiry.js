import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import stringsoflanguages from '../language';

const Open = () => {
  const navigation = useNavigation();
  const {_myEnquiry} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <ScrollView>
        <TouchableOpacity onPress={() => navigation.navigate('XyzJob')}>
          <View style={styles.container}>
            <Text style={styles.text}>{_myEnquiry.xyzJob}</Text>
            <Text style={styles.subText}>{_myEnquiry.subTitle}</Text>
            <DashLine />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View>
                <Text style={styles.date}>{_myEnquiry.startDate}</Text>
                <Text style={styles.subDate}>12/12/2021</Text>
              </View>

              <View>
                <Text style={styles.date}>{_myEnquiry.endDate}</Text>
                <Text style={styles.subDate}>12/12/2021</Text>
              </View>
            </View>
            <DashLine />

            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View>
                <Text style={styles.date}>{_myEnquiry.project}</Text>
                <Text style={styles.subDate}>{_myEnquiry.interior}</Text>
              </View>

              <View>
                <Text style={styles.date}>{_myEnquiry.jobLocation}</Text>
                <Text style={styles.subDate}>{_myEnquiry.address}</Text>
              </View>
            </View>
            <DashLine />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View>
                <Text style={styles.date}>{_myEnquiry.site}</Text>
                <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
              </View>

              <View>
                <Text style={styles.date}>{_myEnquiry.transport}</Text>
                <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>

        <View style={styles.container}>
          <Text style={styles.text}>{_myEnquiry.xyzJob}</Text>
          <Text style={styles.subText}>{_myEnquiry.subTitle}</Text>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.endDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>
          </View>
          <DashLine />

          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.project}</Text>
              <Text style={styles.subDate}>{_myEnquiry.interior}</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.jobLocation}</Text>
              <Text style={styles.subDate}>{_myEnquiry.address}</Text>
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.site}</Text>
              <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.transport}</Text>
              <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const Awarded = () => {
  const {_myEnquiry} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <ScrollView>
        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>{_myEnquiry.xyzJob}</Text>
              <Text style={styles.subBooking}>{_myEnquiry.subTitle}</Text>
            </View>
            <Image
              style={{
                width: 20,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: 10,
              }}
              source={require('../images/medal.png')}
            />
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.endDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>
          </View>
          <DashLine />

          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.project}</Text>
              <Text style={styles.subDate}>{_myEnquiry.interior}</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.jobLocation}</Text>
              <Text style={styles.subDate}>{_myEnquiry.address}</Text>
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.site}</Text>
              <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.transport}</Text>
              <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>{_myEnquiry.xyzJob}</Text>
              <Text style={styles.subBooking}>{_myEnquiry.subTitle}</Text>
            </View>
            <Image
              style={{
                width: 20,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: 10,
              }}
              source={require('../images/medal.png')}
            />
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.endDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>
          </View>
          <DashLine />

          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.project}</Text>
              <Text style={styles.subDate}>{_myEnquiry.interior}</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.jobLocation}</Text>
              <Text style={styles.subDate}>{_myEnquiry.address}</Text>
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View>
              <Text style={styles.date}>{_myEnquiry.site}</Text>
              <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
            </View>

            <View>
              <Text style={styles.date}>{_myEnquiry.transport}</Text>
              <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const Completed = () => {
  const {_myEnquiry} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <View style={styles.container}>
        <Text style={styles.text}>{_myEnquiry.xyzJob}</Text>
        <Text style={styles.subText}>{_myEnquiry.subTitle}</Text>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View>
            <Text style={styles.date}>{_myEnquiry.startDate}</Text>
            <Text style={styles.subDate}>12/12/2021</Text>
          </View>

          <View>
            <Text style={styles.date}>{_myEnquiry.endDate}</Text>
            <Text style={styles.subDate}>12/12/2021</Text>
          </View>
        </View>
        <DashLine />

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View>
            <Text style={styles.date}>{_myEnquiry.project}</Text>
            <Text style={styles.subDate}>{_myEnquiry.interior}</Text>
          </View>

          <View>
            <Text style={styles.date}>{_myEnquiry.jobLocation}</Text>
            <Text style={styles.subDate}>{_myEnquiry.address}</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View>
            <Text style={styles.date}>{_myEnquiry.site}</Text>
            <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
          </View>

          <View>
            <Text style={styles.date}>{_myEnquiry.transport}</Text>
            <Text style={styles.subDate}>{_myEnquiry.yes}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const renderScene = SceneMap({
  first: Open,
  second: Awarded,
  third: Completed,
});

const MyJobs = () => {
  const {_myEnquiry} = stringsoflanguages;
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: _myEnquiry.Open},
    {key: 'second', title: _myEnquiry.Awarded},
    {key: 'third', title: _myEnquiry.Completed},
  ]);
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={_myEnquiry.title} />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            tabStyle={{}}
            scrollEnabled={false}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default MyJobs;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 5,
    marginTop: 20,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 12,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    marginHorizontal: 10,
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    // marginHorizontal: 20,
    marginTop: 5,
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    // marginHorizontal: 20,
    marginTop: 5,
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 10,
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 50,
    marginTop: 20,
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
