
import React, { useState, useEffect, useRef } from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput
} from 'react-native';
import { StatusBarDark } from '../../Custom/CustomStatusBar';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AppHeader from '../../Custom/CustomAppHeader';
import { ButtonStyle } from '../../Custom/CustomView';

const WorkerHelp = ({ navigation, route }) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const input_name = useRef(null)
    const input_email = useRef(null)
    const input_message = useRef(null)

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
        });
        return unsubscribe;
    }, [navigation]);

    return (
        <View style={styles.continer}>
            <StatusBarDark bg='#FFF' />
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={require('../../images/e-remove.png')}
                title={'Help'}
            />
            <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: 25 }}>
                    <View style={{ paddingVertical: 20, alignItems: 'center' }}>
                        <View style={styles.userImgViewOffCss}>
                            <Image style={styles.userImgOffCss} source={require('../../images/support1.png')} />
                        </View>
                        <Text style={styles.userNameTextOffCss}>Need Some Help?</Text>
                    </View>
                    <View style={styles.inputViewOffCss}>
                        <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 28, height: 21, resizeMode: 'contain' }} source={require('../../images/envelopes.png')} />
                        </View>
                        <View style={{ flex: 1, paddingHorizontal: 10 }}>
                            <Text style={styles.inputTextOffCss}>Email</Text>
                            <Text style={[styles.inputTextOffCss, { color: '#000', fontSize: 18 }]}>support@labourhome.com</Text>
                        </View>
                    </View>
                    <View style={styles.inputViewOffCss}>
                        <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 28, height: 28, resizeMode: 'contain' }} source={require('../../images/phone-call.png')} />
                        </View>
                        <View style={{ flex: 1, paddingHorizontal: 10 }}>
                            <Text style={styles.inputTextOffCss}>Phone</Text>
                            <Text style={[styles.inputTextOffCss, { color: '#000', fontSize: 18 }]}>1800784673</Text>
                        </View>
                    </View>
                    <View style={styles.getInTouchCardViewOffCss}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image style={{ width: 30, height: 30, resizeMode: 'contain' }} source={require('../../images/help1.png')} />
                            <Text style={[styles.userNameTextOffCss, { marginVertical: 0, marginLeft: 10 }]}>Get in Touch</Text>
                        </View>
                        <Text style={styles.hoursToAddTextOffCss}>Please give us in between 12 to 24 working hours to address your issues.</Text>
                        <View style={{ marginBottom: 15 }}>
                            <Text style={[styles.inputTextOffCss, { color: '#ACB1C0' }]}>Name</Text>
                            <View style={{ borderBottomColor: '#C8C8D3', borderBottomWidth: 0.4 }}>
                                <TextInput
                                    value={name}
                                    returnKeyType={'next'}
                                    keyboardType={'default'}
                                    style={styles.inputTypTextOffCss}
                                    onChangeText={(text) => { setName(text) }}
                                    onSubmitEditing={() => input_email.current.focus()}
                                    ref={input_name}
                                />
                            </View>
                        </View>
                        <View style={{ marginBottom: 15 }}>
                            <Text style={[styles.inputTextOffCss, { color: '#ACB1C0' }]}>Email</Text>
                            <View style={{ borderBottomColor: '#C8C8D3', borderBottomWidth: 0.4 }}>
                                <TextInput
                                    value={email}
                                    returnKeyType={'next'}
                                    keyboardType={'default'}
                                    style={styles.inputTypTextOffCss}
                                    onChangeText={(text) => { setEmail(text) }}
                                    onSubmitEditing={() => input_message.current.focus()}
                                    ref={input_email}
                                />
                            </View>
                        </View>
                        <View style={{ marginBottom: 15 }}>
                            <Text style={[styles.inputTextOffCss, { color: '#ACB1C0' }]}>Message</Text>
                            <View style={{ borderBottomColor: '#C8C8D3', borderBottomWidth: 0.4 }}>
                                <TextInput
                                    value={message}
                                    multiline={true}
                                    ref={input_message}
                                    returnKeyType={'done'}
                                    keyboardType={'default'}
                                    textAlignVertical={'top'}
                                    placeholder='type your message here……'
                                    onChangeText={(text) => { setMessage(text) }}
                                    style={[styles.inputTypTextOffCss, { height: 89 }]}
                                />
                            </View>
                        </View>
                        <View style={{ marginVertical: 20 }}>
                            <ButtonStyle
                                marginHorizontal={1}
                                title={'SUBMIT'}
                                onPress={() => { navigation.goBack() }}
                            />
                        </View>
                    </View>

                </View>
            </ScrollView>
        </View>
    );
};

export default WorkerHelp;

const styles = StyleSheet.create({
    continer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    userImgViewOffCss: {
        height: 150, width: 150, justifyContent: 'center', alignItems: 'center',
        marginTop: hp(2.50),
    },
    userImgOffCss: {
        height: "100%", width: "100%", resizeMode: 'contain',
    },
    userNameTextOffCss: {
        fontFamily: 'Avenir-Heavy', fontSize: hp(2.75), fontWeight: '900',
        color: '#1E2432', letterSpacing: 0.9, marginVertical: 10
    },
    inputViewOffCss: {
        flexDirection: 'row', backgroundColor: '#FFF', elevation: 2, borderRadius: 12,
        paddingVertical: 15, marginVertical: 10
    },
    inputTextOffCss: {
        fontFamily: 'Avenir-Meduim', fontSize: hp(2.20), fontWeight: '400',
        color: '#00000060'
    },
    getInTouchCardViewOffCss: {
        backgroundColor: '#FFF', elevation: 2, borderRadius: 10,
        paddingHorizontal: 15, paddingVertical: 10, marginVertical: 10
    },
    hoursToAddTextOffCss: {
        fontFamily: 'Avenir-Meduim', fontSize: hp(1.90), fontWeight: '400',
        color: '#7D7D7E', marginVertical: 10, marginBottom: 15
    },
    inputTypTextOffCss: {
        height: 40, flex: 1, fontFamily: 'Avenir-Meduim', fontSize: hp(2.20), fontWeight: '400',
        color: '#1E2432'
    }
});
