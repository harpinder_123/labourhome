import React, { useEffect, useState, useCallback, useRef } from 'react';
import {
    ScrollView, ImageBackground, StyleSheet, Text, View, Image,
    Switch, TouchableOpacity, FlatList
} from 'react-native';
import moment from 'moment';
import AppHeader from '../../Custom/CustomAppHeader';
import { StatusBarDark } from '../../Custom/CustomStatusBar';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { ButtonStyle } from '../../Custom/CustomView';
import CustomModal from '../../Custom/CustomModal';

const WorkerHome = ({ navigation, route }) => {
    const [isEnabled, setIsEnabled] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);
    const [videoTutorialData, setVideoTutorialData] = useState([
        { title: 'How to find and submit quotation?', timeing: '10 min' }, { title: 'How to find and submit quotation?', timeing: '15 min' },
        { title: 'How to find and submit quotation?', timeing: '10 min' }, { title: 'How to find and submit quotation?', timeing: '15 min' },
    ])

    useEffect(() => {
        const willFocusSubscription = navigation.addListener('focus', () => {
        });
        return willFocusSubscription;
    }, [navigation])

    const toggleSwitch = () => {
        setIsEnabled(previousState => !previousState)
    }

    const acceptModal = (visible) => {
        setModalOpen(visible)
    }

    const jobRequestsAccept = () => {
        acceptModal(false);
        navigation.navigate('WorkerMyBooking')
    }

    const videosTutorialList = ({ item, index }) => {
        return (
            <View style={{ width: wp(40), marginLeft: 14 }}>
                <TouchableOpacity activeOpacity={0.8}>
                    <ImageBackground
                        style={styles.videoIconOffCss}
                        source={require('../../images/Group2.png')}>
                        <Image
                            style={styles.playIconOffCss}
                            source={require('../../images/play.png')}
                        />
                    </ImageBackground>
                </TouchableOpacity>
                <Text style={styles.playtext}>{item.title}</Text>
                <Text style={styles.subplaytext}>{item.timeing}</Text>
            </View>
        )
    }

    moment.locale('en');
    var dateTime = "Monday, 2 Sep 21 10:30 am"
    return (
        <View style={styles.continer}>
            <StatusBarDark bg='#FFF' />
            <AppHeader
                title={'Hello, Robert'}
                smailTitle={'Worker'}
                titlePaddingHorizontal={8}
                elevation={0.100}
                onClickSmailTitle={() => { alert('Worker') }}
                leftIcon={require('../../images/home_logo.png')}
                alrm={require('../../images/notification.png')}
                alrmOnClick={() => { }}
                leftOnClick={() => { }}
            />
            <ScrollView nestedScrollEnabled={true} contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}>
                <View style={styles.newJobViewOffCss}>
                    <Text style={[styles.newJobTextOffCss, { fontWeight: '900', fontSize: hp(2) }]}>15+
                        <Text style={styles.newJobTextOffCss}> Plumbers got the job today in your area</Text></Text>
                </View>
                <View style={{ flex: 1, paddingVertical: 15 }}>
                    <View style={styles.availabilityStatusViewOffCss}>
                        <View style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 20 }}>
                            <Text style={styles.availabliStatusTextOffCss}>Availability status</Text>
                            <Text style={styles.availabliTextOffCss}>Available</Text>
                        </View>
                        <View style={{ flex: 0.4, justifyContent: 'center', alignItems: 'center' }}>
                            <Switch
                                trackColor={{ false: "#767577", true: "#4DD865" }}
                                thumbColor={isEnabled ? "#FFF" : "#FFF"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={() => { toggleSwitch() }}
                                value={isEnabled}
                                style={{ transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }] }}
                            />
                        </View>
                    </View>
                    <Text style={styles.jobRequestsHeadingTextOffCss}>Job Requests</Text>
                    <View style={styles.jobRequestsViewCardOffCss}>
                        <View style={{ paddingVertical: 10, borderBottomColor: '#C8C8D3', borderBottomWidth: 0.3 }}>
                            <Text style={styles.bookingIDTextOffCss}>Booking #123</Text>
                            <Text style={styles.dateTimeTextOffCss}>{dateTime}</Text>
                            {/* {moment(dateTime).format("MMMM Do YYYY, h:mm:ss a")} */}
                        </View>
                        <View style={{ paddingVertical: 10, flexDirection: 'row', alignItems: 'center', borderBottomColor: '#C8C8D3', borderBottomWidth: 0.4 }}>
                            <Image style={styles.jobUserImgOffCss} source={require('../../images/pic.png')} />
                            <Text style={styles.jobUserNameTextOffCss}>Rahul Malhotra</Text>
                        </View>
                        <View style={{ paddingVertical: 10 }}>
                            <Text style={[styles.jobUserNameTextOffCss, { fontSize: hp(1.65), marginLeft: 0, marginBottom: 5 }]}>Site Address</Text>
                            <Text style={styles.siteAddreTextOffCss}>1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash Place, Pitam Pura, New Delhi 110034 (IN)</Text>
                        </View>
                        <View style={{ paddingVertical: 10, marginBottom: 5 }}>
                            <ButtonStyle
                                height={hp(4.10)}
                                fontSize={hp(1.65)}
                                title={'ACCEPT'}
                                bgColor={'#6DD400'}
                                marginHorizontal={wp(25)}
                                onPress={() => { acceptModal(true) }}
                            />
                        </View>
                    </View>
                    <CustomModal
                        buttonTitle={'OK'}
                        titleFontSize={22}
                        buttonRadius={17.5}
                        buttonHeight={hp(5)}
                        openVisible={modalOpen}
                        buttonWidth={wp(30.50)}
                        buttonfontSize={hp(1.90)}
                        title={'Request Accepted'}
                        modalImg={require('../../images/mark.png')}
                        openModal={(value) => { acceptModal(value) }}
                        onclickPress={() => { jobRequestsAccept() }}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 15 }}>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() => { navigation.navigate('WorkerMyBooking') }}
                            style={[styles.bookingViewOffCss, { marginRight: 10 }]}>
                            <Image style={styles.groupBookingImgOffCss} source={require('../../images/Group.png')} />
                            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                <Text style={styles.bookingTextOffCss}>My Bookings</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() => { navigation.navigate('WorkerMyAccount')}}
                            style={styles.bookingViewOffCss}>
                            <Image style={styles.groupBookingImgOffCss} source={require('../../images/Group_3.png')} />
                            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                <Text style={styles.bookingTextOffCss}>My Account</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 15 }}>
                        <Text style={styles.videoTutorialTextOffCss}>Video Tutorial</Text>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.showAllTextOffCss}>Show all</Text>
                            <Image style={styles.showAllIconOffCss} source={require('../../images/video.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <FlatList
                            horizontal
                            data={videoTutorialData}
                            renderItem={videosTutorialList}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    <View style={styles.priceJobChangeViewOffCss}>
                        <View style={styles.proceJovRowViewOffCss}>
                            <View style={{ flex: 0.1 }}>
                                <Image style={styles.infoImgOffCss} source={require('../../images/info.png')} />
                            </View>
                            <View style={{ flex: 1, paddingLeft: 10 }}>
                                <Text style={[styles.newJobTextOffCss, { color: '#F2AD4B', fontSize: hp(1.65) }]}>The average price for a plumber job in your area is</Text>
                                <Text style={[styles.bookingTextOffCss, { color: '#F2AD4B', paddingVertical: 5 }]}>₹500/day</Text>
                            </View>
                        </View>
                        <Text style={[styles.newJobTextOffCss, { color: '#E02020', fontSize: hp(1.45) }]}>*Change your price to get more jobs</Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

export default WorkerHome;

const styles = StyleSheet.create({
    continer: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    newJobViewOffCss: {
        height: hp(5.50), backgroundColor: '#6CBDFF', paddingHorizontal: 30, justifyContent: 'center',
        alignItems: 'center'
    },
    newJobTextOffCss: {
        color: '#FFFFFF', fontSize: hp(1.90), fontFamily: 'Avenir-Meduim', fontWeight: '500'
    },
    availabilityStatusViewOffCss: {
        backgroundColor: '#F2AD4B', height: hp(11), borderRadius: 8, flexDirection: 'row',
        justifyContent: 'space-between', marginHorizontal: 15,
    },
    availabliStatusTextOffCss: {
        color: '#FFFFFF', fontSize: hp(2.50), fontFamily: 'Avenir-Meduim', fontWeight: '500', marginBottom: 5
    },
    availabliTextOffCss: {
        color: '#FFFFFF', fontSize: hp(2.20), fontFamily: 'Avenir-Meduim', fontWeight: '900'
    },
    jobRequestsHeadingTextOffCss: {
        color: '#454545', fontSize: hp(2.50), fontFamily: 'Avenir-Heavy', fontWeight: '900',
        marginVertical: 10, marginHorizontal: 15,
    },
    jobRequestsViewCardOffCss: {
        backgroundColor: '#FFF', elevation: 1.5, borderRadius: 8, paddingHorizontal: 15, marginVertical: 10,
        marginHorizontal: 15,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.7,
        //   shadowRadius: 60,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    bookingIDTextOffCss: {
        color: '#454545', fontSize: hp(1.90), fontFamily: 'Avenir-Heavy', fontWeight: '900', marginBottom: 2
    },
    dateTimeTextOffCss: {
        color: '#A5A5A5', fontSize: hp(1.40), fontFamily: 'Avenir-Meduim', fontWeight: '500'
    },
    jobUserImgOffCss: {
        width: wp(13.90), height: hp(6.90), borderRadius: 50 / 2, resizeMode: 'contain'
    },
    jobUserNameTextOffCss: {
        color: '#454545', fontSize: hp(1.90), fontFamily: 'Avenir-Heavy', fontWeight: '900', marginLeft: 20
    },
    siteAddreTextOffCss: {
        color: '#333333', fontSize: hp(1.65), fontFamily: 'Avenir-Meduim', fontWeight: '500', opacity: 0.6
    },
    bookingViewOffCss: {
        width: wp(44), height: 120, backgroundColor: '#FFF', elevation: 3, marginBottom: 5,
        borderRadius: 8, marginVertical: 10, paddingHorizontal: 15, paddingVertical: 10,
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.7,
        //   shadowRadius: 60,
        shadowColor: Platform.OS == 'ios' ? "#00000060" : '#000000',
    },
    groupBookingImgOffCss: {
        width: wp(13.90), height: hp(6.90), resizeMode: 'contain'
    },
    bookingTextOffCss: {
        color: '#454545', fontSize: hp(2), fontFamily: 'Avenir-Heavy', fontWeight: '900',
    },
    videoTutorialTextOffCss: {
        color: '#454545', fontSize: hp(2.50), fontFamily: 'Avenir-Heavy', fontWeight: '900',
        marginVertical: 10
    },
    showAllTextOffCss: {
        color: '#6CBDFF', fontSize: hp(1.90), fontFamily: 'Avenir-Meduim', fontWeight: '500'
    },
    showAllIconOffCss: {
        width: 6, height: 10, resizeMode: 'contain', marginLeft: 5
    },
    priceJobChangeViewOffCss: {
        height: hp(11), backgroundColor: '#FDEED9', borderRadius: 8, borderWidth: 0.7,
        borderColor: '#F2AD4B', paddingHorizontal: 10, paddingVertical: 10, marginHorizontal: 15,
    },
    proceJovRowViewOffCss: {
        flexDirection: 'row',
    },
    infoImgOffCss: {
        width: wp(8), height: hp(3.60), resizeMode: 'contain', tintColor: '#F2AD4B'
    },
    videoIconOffCss: {
        width: wp(40), height: hp(15), backgroundColor: '#00000050',
        elevation: 5, resizeMode: 'contain', borderRadius: 15
    },
    playIconOffCss: {
        width: wp(8), height: hp(4), resizeMode: 'contain', alignSelf: 'center', marginTop: hp(5)
    },
    playtext: {
        fontFamily: 'Avenir-Heavy', fontSize: hp(1.75), fontWeight: 'bold', color: '#333333', paddingVertical: 5,
    },
    subplaytext: {
        fontFamily: 'Avenir-Heavy', fontSize: hp(1.60), fontWeight: 'bold', color: '#33333390',
    },
});
