import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Modal,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
  FlatList,
} from 'react-native';
import moment from 'moment';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import AppHeader from '../../Custom/CustomAppHeader';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';

const Jobs = () => {
  const navigation = useNavigation();
  const [jobsData, setJobsData] = useState([
    {
      bookingId: '123',
      condetion: true,
      userName: 'Rahul Malhotra',
      address:
        '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash Place, Pitam Pura, New Delhi 110034 (IN)',
    },
    {
      bookingId: '321',
      condetion: false,
      userName: 'Rahul Malhotra',
      address:
        '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash Place, Pitam Pura, New Delhi 110034 (IN)',
    },
    {
      bookingId: '987',
      condetion: false,
      userName: 'Rahul Malhotra',
      address:
        '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash Place, Pitam Pura, New Delhi 110034 (IN)',
    },
    {
      bookingId: '015',
      condetion: false,
      userName: 'Rahul Malhotra',
      address:
        '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash Place, Pitam Pura, New Delhi 110034 (IN)',
    },
  ]);
  const [modalVisible, setModalVisible] = useState(false);
  const [selctModalJob, setSelctModalJob] = useState('');
  const [otp, setOtp] = useState('');

  const openOtpJobModal = (visible, value) => {
    setModalVisible(visible);
    setSelctModalJob(value);
  };

  const jobverfyfunction = value => {
    if (value == 'endjob') {
      navigation.navigate('Rating');
      openOtpJobModal(false);
    } else {
      openOtpJobModal(false);
    }
  };

  const jobOtpModalfunction = () => {
    return (
      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={() => {
          // Alert.alert("Modal has been closed.");
          openOtpJobModal(false);
        }}>
        <View style={styles.modal_View}>
          <View style={styles.mdtop}>
            <Text style={styles.modalTitleText}>Enter OTP to end job</Text>
            <OTPInputView
              style={styles.otpInput}
              pinCount={4}
              onCodeChanged={text => {
                setOtp(text.replace(/[^0-9]/g, ''));
              }}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
            />
            <View style={{marginVertical: 10}}>
              <ButtonStyle
                height={hp(5)}
                fontSize={hp(1.9)}
                title={'OK'}
                bgColor={'#6CBDFF'}
                marginHorizontal={wp(29)}
                onPress={() => {
                  jobverfyfunction(selctModalJob);
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  const jobsList = ({item, index}) => {
    moment.locale('en');
    var dateTime = 'Monday, 2 Sep 21 10:30 am';
    return (
      <View style={styles.jobRequestsViewCardOffCss}>
        <View
          style={{
            paddingVertical: 10,
            flexDirection: 'row',
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.3,
          }}>
          <View style={{flex: 0.8}}>
            <Text style={styles.bookingIDTextOffCss}>
              Booking #{item.bookingId}
            </Text>
            <Text style={styles.dateTimeTextOffCss}>{dateTime}</Text>
          </View>
          {item.condetion === true && (
            <View style={{flex: 0.3}}>
              <View style={styles.onGoingViewOffCss}>
                <Text style={styles.onGoingTextOffCss}>Ongoing</Text>
              </View>
            </View>
          )}
          {/* {moment(dateTime).format("MMMM Do YYYY, h:mm:ss a")} */}
        </View>
        <View
          style={{
            paddingVertical: 10,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.4,
          }}>
          <Image
            style={styles.jobUserImgOffCss}
            source={require('../../images/pic.png')}
          />
          <Text style={styles.jobUserNameTextOffCss}>{item.userName}</Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity activeOpacity={0.7} onPress={() => {}}>
              <Image
                style={{width: 30, height: 30, resizeMode: 'contain'}}
                source={require('../../images/phone.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{paddingVertical: 10}}>
          <Text
            style={[
              styles.jobUserNameTextOffCss,
              {fontSize: hp(1.65), marginLeft: 0, marginBottom: 5},
            ]}>
            Site Address
          </Text>
          <Text style={styles.siteAddreTextOffCss}>{item.address}</Text>
        </View>
        <View style={{paddingVertical: 10, marginBottom: 5}}>
          {item.condetion === true ? (
            <ButtonStyle
              height={hp(4.8)}
              fontSize={hp(1.9)}
              title={'END JOB'}
              bgColor={'#E02020'}
              marginHorizontal={wp(22)}
              onPress={() => {
                openOtpJobModal(true, 'endjob');
              }}
            />
          ) : (
            <ButtonStyle
              height={hp(4.8)}
              fontSize={hp(1.9)}
              title={'START JOB'}
              bgColor={'#6DD400'}
              marginHorizontal={wp(22)}
              onPress={() => {
                openOtpJobModal(true, 'startjob');
              }}
            />
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={[styles.container, {paddingVertical: 10}]}>
      <FlatList
        data={jobsData}
        renderItem={jobsList}
        showsVerticalScrollIndicator={false}
      />
      {jobOtpModalfunction()}
    </View>
  );
};

const Completed = () => {
  const navigation = useNavigation();
  const [jobsData, setJobsData] = useState([
    {
      bookingId: '123',
      condetion: true,
      userName: 'Rahul Malhotra',
      address:
        '1007, 10th Floor, Tower-1, Pearls Omaxe Building, Netaji Subhash Place, Pitam Pura, New Delhi 110034 (IN)',
      customerMess:
        '“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.”',
    },
  ]);

  const jobsList = ({item, index}) => {
    moment.locale('en');
    var dateTime = 'Monday, 2 Sep 21 10:30 am';
    return (
      <View style={styles.jobRequestsViewCardOffCss}>
        <View
          style={{
            paddingVertical: 10,
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.3,
          }}>
          <Text style={styles.bookingIDTextOffCss}>
            Booking #{item.bookingId}
          </Text>
          <Text style={styles.dateTimeTextOffCss}>{dateTime}</Text>
          {/* {moment(dateTime).format("MMMM Do YYYY, h:mm:ss a")} */}
        </View>
        <View
          style={{
            paddingVertical: 10,
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.4,
          }}>
          <Image
            style={styles.jobUserImgOffCss}
            source={require('../../images/pic.png')}
          />
          <Text style={styles.jobUserNameTextOffCss}>{item.userName}</Text>
          <View style={{flex: 1, alignItems: 'flex-end'}}>
            <TouchableOpacity activeOpacity={0.7} onPress={() => {}}>
              <Image
                style={{width: 30, height: 30, resizeMode: 'contain'}}
                source={require('../../images/phone.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            paddingVertical: 10,
            borderBottomColor: '#C8C8D3',
            borderBottomWidth: 0.4,
          }}>
          <Text
            style={[
              styles.jobUserNameTextOffCss,
              {fontSize: hp(1.65), marginLeft: 0, marginBottom: 5},
            ]}>
            Site Address
          </Text>
          <Text style={styles.siteAddreTextOffCss}>{item.address}</Text>
        </View>
        <View style={{paddingVertical: 10}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 5,
            }}>
            <Text
              style={[
                styles.jobUserNameTextOffCss,
                {fontSize: hp(1.9), marginLeft: 0, marginBottom: 5},
              ]}>
              Customer Rating
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.bookingIDTextOffCss, {marginRight: 5}]}>
                4.0
              </Text>
              <FlatList
                horizontal
                data={['', '', '', '', '']}
                renderItem={(item, index) => {
                  return (
                    <Image
                      style={{
                        width: 16.54,
                        height: 16.54,
                        resizeMode: 'contain',
                      }}
                      source={require('../../images/star.png')}
                    />
                  );
                }}
              />
            </View>
          </View>
          <Text style={styles.siteAddreTextOffCss}>{item.customerMess}</Text>
        </View>
      </View>
    );
  };

  return (
    <View style={[styles.container, {paddingVertical: 10}]}>
      <FlatList
        data={jobsData}
        renderItem={jobsList}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

const renderScene = SceneMap({
  first: Jobs,
  second: Completed,
});

const WorkerMyBooking = () => {
  const navigation = useNavigation();
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Jobs'},
    {key: 'second', title: 'Completed'},
  ]);

  return (
    <View style={styles.container}>
      <StatusBarDark />
      <AppHeader
        elevation={0.1}
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../../images/e-remove.png')}
        title={'My Bookings'}
      />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.tabstyle}
            labelStyle={styles.labelStyle}
            scrollEnabled={false}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default WorkerMyBooking;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
  },
  tabstyle: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
  },
  labelStyle2: {
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  jobRequestsViewCardOffCss: {
    backgroundColor: '#FFF',
    elevation: 1.5,
    borderRadius: 8,
    paddingHorizontal: 15,
    marginVertical: 5,
    marginHorizontal: 15,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.7,
    //   shadowRadius: 60,
    shadowColor: Platform.OS == 'ios' ? '#00000060' : '#000000',
  },
  bookingIDTextOffCss: {
    color: '#454545',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginBottom: 2,
  },
  dateTimeTextOffCss: {
    color: '#A5A5A5',
    fontSize: hp(1.4),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  jobUserImgOffCss: {
    width: wp(13.9),
    height: hp(6.9),
    borderRadius: 50 / 2,
    resizeMode: 'contain',
  },
  jobUserNameTextOffCss: {
    color: '#454545',
    fontSize: hp(1.9),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    marginLeft: 20,
  },
  siteAddreTextOffCss: {
    color: '#333333',
    fontSize: hp(1.65),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    opacity: 0.6,
  },
  onGoingViewOffCss: {
    padding: 4,
    backgroundColor: '#FA6400',
    borderRadius: 12.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  onGoingTextOffCss: {
    color: '#FFFFFF',
    fontSize: hp(1.65),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
  },
  modal_View: {
    backgroundColor: '#000000bb',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mdtop: {
    width: 330,
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderRadius: 20,
    paddingVertical: 10,
  },
  modalTitleText: {
    fontSize: hp(2.5),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    color: '#333333',
    textAlign: 'center',
    paddingVertical: 10,
  },
  otpInput: {
    height: 60,
    marginVertical: 20,
    marginHorizontal: 30,
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#F4F4F4',
    fontWeight: 'bold',
    color: '#100C08',
    backgroundColor: '#00000019',
    borderRadius: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
  },
  underlineStyleHighLighted: {
    width: 60,
    height: 60,
    borderRadius: 20,
    fontFamily: 'Avenir-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#00000019',
  },
});

const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
