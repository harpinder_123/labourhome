
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    TextInput,
    FlatList,
    ImageBackground
} from 'react-native';
import { StatusBarDark } from '../../Custom/CustomStatusBar';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AppHeader from '../../Custom/CustomAppHeader';

const WorkerProfile = ({ navigation, route }) => {
    const [photoVideoData, setPhotoVideo] = useState([
        { photos: require('../../images/photo.png'), path: 'png' }, { photos: require('../../images/photo.png'), path: 'mp4' },
        { photos: require('../../images/photo.png'), path: 'png' }, { photos: require('../../images/photo.png'), path: 'mp4' },
        { photos: require('../../images/photo.png'), path: 'png' }, { photos: require('../../images/photo.png'), path: 'mp4' },
    ])
    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
        });
        return unsubscribe;
    }, [navigation]);

    const photoVedioList = ({ item, index }) => {
        return (
            <>
                {item.path === 'mp4' ?
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ marginVertical: 10 }}>
                        <ImageBackground
                            style={styles.photoVideoImgOffCss}
                            source={item.photos}>
                            <Image
                                style={{ width: 30, height: 40, resizeMode: 'contain' }}
                                source={require('../../images/play-button.png')}
                            />
                        </ImageBackground>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                        activeOpacity={0.7}
                        style={{ marginVertical: 10, marginRight: 5, marginLeft: 5, }}>
                        <Image style={styles.photoVideoImgOffCss} source={item.photos} />
                    </TouchableOpacity>
                }
            </>
        )
    }

    return (
        <View style={styles.continer}>
            <StatusBarDark bg='#FFF' />
            <AppHeader
                backOnClick={() => { navigation.goBack() }}
                backIcon={require('../../images/e-remove.png')}
                title={'My Profile'}
                search={require('../../images/edit_icon.png')}
                searchIcontintColor={'#000'}
                searchOnClick={() => { navigation.navigate('WorkerEditProfile') }}
            />
            <ScrollView contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: 25 }}>
                    <View style={{ paddingVertical: 20, alignItems: 'center' }}>
                        <View style={styles.userImgViewOffCss}>
                            <Image style={styles.userImgOffCss} source={require('../../images/pic.png')} />
                        </View>
                        <Text style={styles.userNameTextOffCss}>Pramod Kumar</Text>
                        <Text style={styles.containersubtext}>Plumber</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>My Name</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>Rahul Malhotra</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>Phone Number</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>+91 9876546382</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>Gender</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>Male</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>Skill</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>Plumber</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>Daily Rate</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>₹500</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>Hourly Rate*</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>₹200</Text>
                    </View>
                    <View style={styles.detaiViewOffCss}>
                        <Text style={styles.detaiTextOffCss}>Experience</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56' }]}>10 yrs</Text>
                    </View>
                    <View style={{ paddingVertical: 15 }}>
                        <Text style={styles.detaiTextOffCss}>Bio</Text>
                        <Text style={[styles.detaiTextOffCss, { color: '#2A3B56', marginTop: 5 }]}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</Text>
                    </View>
                </View>
                <View style={{ paddingVertical: 0 }}>
                    <Text style={[styles.detaiTextOffCss, { paddingHorizontal: 25 }]}>Photos/Videos</Text>
                    <View style={{ width: wp(100), paddingHorizontal: 10 }}>
                        <FlatList
                            horizontal
                            data={photoVideoData}
                            renderItem={photoVedioList}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

export default WorkerProfile;

const styles = StyleSheet.create({
    continer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    userImgViewOffCss: {
        height: hp(16), width: wp(32), borderRadius: wp(32) / 2, justifyContent: 'center', alignItems: 'center',
        marginVertical: 7
    },
    userImgOffCss: {
        height: "100%", width: "100%", resizeMode: 'contain',
    },
    userNameTextOffCss: {
        fontFamily: 'Avenir-Heavy', fontSize: hp(2.20), fontWeight: '900',
        color: '#2A3B56', paddingVertical: 5, letterSpacing: 0.7
    },
    containersubtext: {
        fontFamily: 'Avenir-Medium', fontWeight: '500', fontSize: hp(1.65), color: '#8A94A3'
    },
    detaiViewOffCss: {
        flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 15
    },
    detaiTextOffCss: {
        fontSize: hp(1.90), fontFamily: 'Avenir-Meduim', fontWeight: '500', color: '#8A94A3'
    },
    photoVideoImgOffCss: {
        width: wp(26), height: hp(12), resizeMode: 'contain', borderRadius: 8,
        justifyContent: 'center', alignItems: 'center'
        // width: wp(27.80), height: hp(13.70), resizeMode: 'contain', borderRadius: 8
    }
});
