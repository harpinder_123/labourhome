import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  TextInput,
} from 'react-native';
import stringsoflanguages from '../../language';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import InputView from '../../Custom/CustomTextInput';
import {BottomView, ButtonStyle} from '../../Custom/CustomView';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Api} from '../../services/Api';
import DeviceInfo from 'react-native-device-info';
import * as actions from '../../redux/actions';
import {useDispatch, useStore} from 'react-redux';

const Login = ({navigation}) => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    mobile: '',
  });
  const loginButtonPress = async () => {
    const {mobile} = state;

    if (mobile.length !== 10) {
      alert('Please Enter Valid Mobile Number');
      return;
    }
    const body = {
      mobile: state.mobile,
      device_id: DeviceInfo.getUniqueId(),
      device_token: 'byFirebase',
      device_type: Platform.OS,
      loginTime: 'left',
      model_name: DeviceInfo.getModel(),
      carrier_name: DeviceInfo.getModel(),
      device_country: 'cntry',
      device_memory: 'memory',
      have_notch: 'have  otch',
      manufacture: 'manufacture',
    };
    const response = await Api.LoginApi(body);
    const {status = false, user_id = '', flag = 0, otp, user_type} = response;
    // alert(JSON.stringify(response, null, 2));
    if (status && user_id !== '') {
      navigation.replace('Otp', {user_id, user_type, mobile, otp});
      actions.Phone(mobile);
    } else {
      alert('Something went wrong');
    }
  };
  const {_login} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <ScrollView>
        <Image style={styles.image} source={require('../../images/logo.png')} />
        <Text style={styles.text}>{_login.title}</Text>
        <Text style={styles.subtext}>{_login.subtitle}</Text>
        <View style={{marginHorizontal: 30}}>
          <InputView
            maxLength={10}
            keyboardType="numeric"
            placeholder={_login.mobile}
            onChangeText={mobile => setState({...state, mobile})}
            value={state.mobile}
          />
        </View>
        <BottomView />
        <ButtonStyle
          title={_login.continue}
          marginHorizontal={30}
          onPress={() => loginButtonPress()}
          // onPress={() => {
          //   navigation.navigate('Otp');
          // }}
        />
        <Text style={styles.sub2text}>{_login.or}</Text>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            padding: 10,
            marginHorizontal: 80,
          }}>
          <TouchableOpacity style={styles.facebook}>
            <Image style={styles.fb} source={require('../../images/fb.png')} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.facebook}>
            <Image
              style={styles.google}
              source={require('../../images/g.png')}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  image: {
    marginTop: hp(11),
    width: wp(42), // 150,
    height: hp(20), // 135,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Avenir',
    fontSize: hp(4.1), // 30,
    fontWeight: '900',
    color: '#454545',
    marginTop: 60,
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9), // 14,
    fontWeight: '500',
    color: '#606E87',
    marginTop: 10,
    marginHorizontal: 30,
    marginBottom: 20,
  },
  sub2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9), // 14,
    fontWeight: '500',
    color: '#454545',
    marginTop: 30,
    textAlign: 'center',
  },
  facebook: {
    width: wp(17), // 60,
    height: 60,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    alignSelf: 'center',
    marginTop: 20,
  },
  fb: {
    width: 20,
    height: 40,
    alignSelf: 'center',
    marginTop: 10,
  },
  google: {
    width: 35,
    height: 40,
    alignSelf: 'center',
    marginTop: 10,
  },
});
