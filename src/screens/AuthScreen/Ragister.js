import React, {useEffect, useState, useCallback, useRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import InputView from '../../Custom/CustomTextInput';
import {RadioButton} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ButtonStyle} from '../../Custom/CustomView';
import CheckBox from '@react-native-community/checkbox';
import Ragister2 from '../Ragister2';
import {useDispatch, useSelector} from 'react-redux';
import {LocalStorage, Api} from '../../services/Api';
import {_SetAuthToken} from '../../services/ApiSauce';
import DeviceInfo from 'react-native-device-info';
import * as actions from '../../redux/actions';
import stringsoflanguages from '../../language';
import RegisterContractor from '../Contractor/RegisterContractor';

let selectGender = [{value: 'Male'}, {value: 'Female'}];

let addressData = [
  {
    title: 'Home',
    address:
      '21/C9, 2nd Floor, Sector-7, Rohini Opp Metro Pillor No. 400, New Delhi - 110085 (IN)',
  },
  {
    title: 'Work',
    address:
      '21/C9, 2nd Floor, Sector-7, Rohini Opp Metro Pillor No. 400, New Delhi - 110085 (IN)',
  },
];

const Ragister = ({navigation, route}) => {
  const {_ragister} = stringsoflanguages;
  const [number, setNumber] = useState();
  const [name, setName] = useState('');
  const [gender, setGender] = useState('');
  const [hourlyRate, setHourlyRate] = useState('');
  const [experience, setExperience] = useState();
  const [aadharNumber, setAadharNumber] = useState();
  const [userType, setUserType] = useState(_ragister.IamCustomer);
  const [selectAddress, setSelectAddress] = useState(0);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [slectTradeText, setSelectTradeText] = useState('');
  const [selectJob, setSelectJob] = useState([
    {value: _ragister.IamWorker},
    {value: _ragister.IamContractor},
    {value: _ragister.IamCustomer},
  ]);
  const [state, setState] = useState({
    user_type: '',
    mobile: '',
    name: '',
    company_name: '',
    project_type: [],
    select_trade: [],
    no_of_worker: [],
    gstin_no: '',
    adhar_number: '',
    pan_no: '',
    email: '',
    address: '',
    city: '',
    pincode: '',
    additional_information: '',
    term: false,
  });
  const registerHandlerCustomerPress = async () => {
    const {
      user_type,
      mobile,
      term,
      name,
      company_name,
      project_type,
      select_trade,
      no_of_worker,
      gstin_no,
      adhar_number,
      pan_no,
      email,
      address,
      city,
      pincode,
      additional_information,
    } = state;
    if (user_type != 2 || user_type != 1 || user_type != 3) {
      alert('Please ! Select User Type');
      return;
    }
    if (name == '') {
      alert('Please ! Enter Your Name');
      return;
    }
    // if (project_type == '') {
    //   alert('Please ! Select User Type');
    //   return;
    // }
    // if (select_trade == '') {
    //   alert('Please ! Select Trade');
    //   return;
    // }
    // if (no_of_worker == '') {
    //   alert('Please ! Select Number of Workers');
    //   return;
    // }
    // if (adhar_number == '') {
    //   alert('Please ! Enter Adhar Number');
    //   return;
    // }
    if (term == false) {
      alert('Please Check Term and Condition');
      return;
    }

    const body = {
      country_code: '+91',
      user_type: 2,
      mobile: mobile,
      name: name,
      company_name: company_name,
      project_type: '',
      select_trade: '',
      no_of_worker: 20,
      gstin_no: gstin_no,
      adhar_number: adhar_number,
      pan_no: pan_no,
      address: address,
      city: city,
      pincode: pincode,
      additional_information: additional_information,
      device_id: DeviceInfo.getUniqueId(),
      device_token: DeviceInfo.getDeviceToken(),
      device_type: Platform.OS,
      loginTime: DeviceInfo.getFirstInstallTime(),
      model_name: DeviceInfo.getModel(),
      carrier_name: DeviceInfo.getModel(),
      device_country: 'device_country',
      device_memory: 'device_memory',
      have_notch: 'have_notch',
      manufacture: 'manufacture',
    };

    const response = await Api.ContractorRegisterApi(body);
    //  alert(JSON.stringify(response, null, 2));
    console.log(JSON.stringify(response, null, 2));
    const {status = false} = response;
    if (status) {
      _SetAuthToken(token);
      LocalStorage.setToken(token);
      navigation.replace('WorkerHome');
    } else {
      alert('Something went wrong');
    }
  };
  const registerPageHandler = () => {
    navigation.navigate('RegisterContractor');
  };

  const dispatch = useDispatch();
  const input_mobile = useRef(null);
  const input_name = useRef(null);
  const input_hourlyRate = useRef(null);
  const input_experience = useRef(null);
  const input_aadharNumber = useRef(null);
  const {selectTrade, phoneNumber} = useSelector(store => store);

  useEffect(() => {
    //  alert(JSON.stringify(phoneNumber));
  }, []);

  useEffect(() => {
    const willFocusSubscription = navigation.addListener('focus', () => {
      LocalStorage.getSelectTrade('selectTrade').then(selectTrade => {
        setSelectTradeText(selectTrade);
      });
    });
    return willFocusSubscription;
  }, [navigation]);

  // const handleClick = useCallback(() => { }, [navigation]);

  const onChangeDropdownUserType = (value, index, data) => {
    setUserType(data[index].value);
  };

  const onChangeDropdownGender = (value, index, data) => {
    setGender(data[index].value);
  };

  const checkAddress = index => {
    setSelectAddress(index);
  };

  const addressList = ({item, index}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          checkAddress(index);
        }}
        style={styles.selectAddViewOffCss}>
        <View
          style={{flex: 0.1, justifyContent: 'center', alignItems: 'center'}}>
          <RadioButton
            value="second"
            color={'#7BAAED'}
            uncheckedColor={'#69707F'}
            status={selectAddress === index ? 'checked' : 'unchecked'}
          />
        </View>
        <View style={{flex: 1, paddingHorizontal: 10}}>
          <Text style={styles.addTitleTextOffCss}>{item.title}</Text>
          <Text style={styles.addAddressTextOffCss}>{item.address}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.continer}>
      <StatusBarDark bg={'#F4F4F4'} />
      <ScrollView
        nestedScrollEnabled={true}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <View style={{height: hp(21), justifyContent: 'flex-end'}}>
          <Text style={styles.text}>{_ragister.title}</Text>
          <Text style={styles.subtext}>{_ragister.subtitle}</Text>
        </View>
        <View style={{flex: 1, paddingVertical: 20}}>
          <View style={{marginTop: '10%', marginBottom: 10}}>
            <Dropdown
              style={styles.drops}
              itemColor={'rgba(0, 0, 0, .54)'}
              underlineColor="transparent"
              label={_ragister.selectUserType}
              //  icon="cheveron-down"
              iconColor="rgba(0, 0, 0, 1)"
              icon={require('../../images/drop.png')}
              dropdownOffset={{top: 32, left: 0}}
              dropdownMargins={{min: 8, max: 16}}
              pickerStyle={{width: '88%', left: '6%', marginTop: 20}}
              dropdownPosition={-3.5}
              shadeOpacity={0.12}
              rippleOpacity={0.4}
              baseColor={'white'}
              data={selectJob}
              onChangeText={(value, index, data) => {
                onChangeDropdownUserType(value, index, data);
              }}
            />
          </View>

          {userType === _ragister.IamWorker ? (
            <>
              <View style={{marginVertical: 5}}>
                <InputView
                  keyboardType="numeric"
                  placeholder={_ragister.number}
                  returnKeyType={'next'}
                  value={number}
                  onChangeText={text => {
                    setNumber(text);
                  }}
                  getFocus={() => input_name.current.focus()}
                  setFocus={input_mobile}
                />
              </View>
              <View style={{marginVertical: 5}}>
                <InputView
                  keyboardType="default"
                  returnKeyType={'next'}
                  placeholder={_ragister.name}
                  value={name}
                  onChangeText={text => {
                    setName(text);
                  }}
                  getFocus={() => input_hourlyRate.current.focus()}
                  setFocus={input_name}
                />
              </View>
              <View style={{marginVertical: 10}}>
                <Dropdown
                  style={styles.drops}
                  itemColor={'rgba(0, 0, 0, .54)'}
                  underlineColor="transparent"
                  label={_ragister.selectGender}
                  icon={require('../../images/drop.png')}
                  iconColor="rgba(0, 0, 0, 1)"
                  shadeOpacity={0.12}
                  rippleOpacity={0.4}
                  baseColor={'white'}
                  data={selectGender}
                  pickerStyle={{width: '88%', left: '6%', marginTop: 20}}
                  dropdownPosition={-2.5}
                  onChangeText={(value, index, data) => {
                    onChangeDropdownGender(value, index, data);
                  }}
                />
              </View>
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => {
                  navigation.navigate('SelectTrade', {
                    selectType: slectTradeText,
                  });
                }}
                style={[
                  styles.drops,
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginVertical: 10,
                  },
                ]}>
                {slectTradeText ? (
                  <>
                    <Text
                      style={{
                        color: '#454545',
                        fontSize: hp(2.2),
                        fontFamily: 'Avenir-Medium',
                        fontWeight: '500',
                      }}>
                      {slectTradeText}
                    </Text>
                    <Image
                      style={styles.dropIconOffCss}
                      source={require('../../images/drop.png')}
                    />
                  </>
                ) : (
                  <>
                    <Text style={styles.tradeTextOffCss}>
                      {_ragister.selectTrade}
                    </Text>
                    <Image
                      style={styles.dropIconOffCss}
                      source={require('../../images/drop.png')}
                    />
                  </>
                )}
              </TouchableOpacity>
              <View style={{marginVertical: 5}}>
                <InputView
                  keyboardType="default"
                  placeholder={_ragister.hourlyRate}
                  returnKeyType={'next'}
                  value={hourlyRate}
                  onChangeText={text => {
                    setHourlyRate(text);
                  }}
                  getFocus={() => input_experience.current.focus()}
                  setFocus={input_hourlyRate}
                />
              </View>
              <View style={{marginVertical: 5}}>
                <InputView
                  keyboardType="default"
                  returnKeyType={'next'}
                  placeholder={_ragister.experience}
                  value={experience}
                  onChangeText={text => {
                    setExperience(text);
                  }}
                  getFocus={() => input_aadharNumber.current.focus()}
                  setFocus={input_experience}
                />
              </View>
              <View style={{marginVertical: 5}}>
                <InputView
                  keyboardType="numeric"
                  returnKeyType={'done'}
                  placeholder={_ragister.aadhaarCardNumber}
                  value={aadharNumber}
                  onChangeText={text => {
                    setAadharNumber(text);
                  }}
                  // onSubmitEditing={() => input_experience.current.focus()}
                  setFocus={input_aadharNumber}
                />
              </View>
              <View style={styles.workLocationViewOffCss}>
                <Text style={styles.workLocaTextOffCss}>
                  {_ragister.workLocation}
                </Text>
                <TouchableOpacity
                  activeOpacity={0.7}
                  style={styles.addLocaViewOffCss}
                  onPress={() => {
                    navigation.navigate('AddAddress');
                  }}>
                  <Text style={styles.addNewTextOffCss}>
                    + {_ragister.addNew}
                  </Text>
                </TouchableOpacity>
              </View>
              <FlatList
                data={addressData}
                renderItem={addressList}
                nestedScrollEnabled={true}
                showsVerticalScrollIndicator={false}
              />
              <View
                style={{
                  flexDirection: 'row',
                  width: wp(90),
                  marginVertical: 10,
                }}>
                <View style={{width: wp(8)}}>
                  <CheckBox
                    disabled={false}
                    value={state.term}
                    onValueChange={term => setState({...state, term})}
                    tintColors={{true: '#F5B04C', false: 'grey'}}
                  />
                </View>
                <View style={{width: wp(82)}}>
                  <Text
                    style={[styles.agreekatlegoOffCss, {paddingVertical: 3}]}>
                    {_ragister.bySigning}
                    <Text style={styles.privacyOffCss}>
                      {' '}
                      {_ragister.termCondition}{' '}
                    </Text>
                    <Text style={styles.agreekatlegoOffCss}>
                      {_ragister.and}
                    </Text>
                    <Text style={styles.privacyOffCss}>
                      {' '}
                      {_ragister.privacyPolicy}
                    </Text>
                  </Text>
                </View>
              </View>
              <View style={{marginVertical: 10}}>
                <ButtonStyle
                  title={_ragister.register}
                  marginHorizontal={1}
                  onPress={() => {
                    navigation.navigate('WorkerHome');
                  }}
                />
              </View>
            </>
          ) : userType === _ragister.IamContractor ? (
            // <Text>I am a contractor</Text>
            <RegisterContractor navigation={navigation} />
          ) : userType === _ragister.IamCustomer ? (
            <Ragister2 navigation={navigation} />
          ) : null}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    paddingHorizontal: 20,
  },
  text: {
    fontFamily: 'Avenir',
    fontSize: hp(4.1), // 30,
    fontWeight: 'bold',
    color: '#454545',
    paddingVertical: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9), // 14,
    fontWeight: '500',
    color: '#606E87',
    lineHeight: 20,
  },
  drops: {
    height: hp(8.1), // 60,
    backgroundColor: '#fff',
    borderRadius: 15,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0,
    paddingHorizontal: 20,
    // elevation: 2,
  },
  tradeTextOffCss: {
    fontSize: hp(2.1),
    fontWeight: '500',
    color: '#7A7A7A',
    fontFamily: 'Avenir-Medium',
  },
  dropIconOffCss: {
    width: wp(7),
    height: hp(5),
    resizeMode: 'contain',
    transform: [{rotate: '270deg'}],
  },
  textinput: {
    height: hp(8.1), // 60,
    width: 330,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    // marginTop: 40,
    // marginHorizontal: 30,
    padding: 20,
  },
  workLocationViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
  },
  workLocaTextOffCss: {
    fontSize: hp(2.2),
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    color: '#454545',
  },
  addLocaViewOffCss: {
    width: wp(25),
    height: hp(4),
    backgroundColor: '#F2AD4B',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addNewTextOffCss: {
    fontSize: hp(1.6),
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    color: '#fff',
  },
  selectAddViewOffCss: {
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    flexDirection: 'row',
    paddingVertical: 10,
    borderRadius: 15,
    marginVertical: 5,
  },
  addTitleTextOffCss: {
    fontSize: hp(2.1),
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    color: '#080040',
    marginBottom: 5,
  },
  addAddressTextOffCss: {
    fontSize: hp(1.7),
    fontFamily: 'Avenir-Meduim',
    fontWeight: '500',
    color: '#8A94A3',
    marginBottom: 5,
  },
  agreekatlegoOffCss: {
    color: '#2D2627',
    fontSize: hp(1.75),
    fontFamily: 'Avenir-Meduim',
    paddingHorizontal: 2,
    letterSpacing: 0.5,
  },
  privacyOffCss: {
    color: '#6CBDFF',
    paddingHorizontal: 5,
    fontSize: hp(1.7),
    fontFamily: 'Avenir-Meduim',
    letterSpacing: 0.5,
  },
});
export default Ragister;
