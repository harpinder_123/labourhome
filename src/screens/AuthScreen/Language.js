import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../../redux/actions';
import {FlatList} from 'react-native-gesture-handler';
import {ButtonStyle} from '../../Custom/CustomView';
import stringsoflanguages from '../../language';
import {LocalStorage} from '../../services/Api';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const Language = ({navigation}) => {
  const [languageSelect] = useState([
    {language: 'English', lang: 'en'},
    {language: 'हिन्दी', lang: 'hi'},
  ]);
  const {language} = useSelector(store => store);
  const dispatch = useDispatch();
  const {_customlang} = stringsoflanguages;
  const changeLanguageHandler = language => {
    stringsoflanguages.setLanguage(language);
    LocalStorage.setLanguage(language);
    dispatch(actions.Language(language));
  };

  const languageFunction = ({item, index}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => {
          changeLanguageHandler(item.lang);
        }}
        style={styles.languageViewOffCss}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Image
            style={styles.subimage}
            source={require('../../images/lang.png')}
          />
          <Text style={styles.texted}>{item.language}</Text>
        </View>
        {language == item.lang ? (
          <View
            style={{flex: 0.1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
              style={styles.tick}
              source={require('../../images/tick.png')}
            />
          </View>
        ) : null}
      </TouchableOpacity>
    );
  };

  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark bg={'#FFFFFF'} />
      <Image
        style={styles.image}
        source={require('../../images/translate.png')}
      />
      <Text style={styles.text}>{_customlang.title}</Text>
      <Text style={styles.subtext}>{_customlang.subtitle}</Text>
      <View style={{marginTop: 10}}>
        <FlatList
          data={languageSelect}
          renderItem={languageFunction}
          showsVerticalScrollIndicator={false}
        />
      </View>
      <View style={{flex: 1, justifyContent: 'flex-end', marginBottom: 20}}>
        <ButtonStyle
          title={_customlang.button}
          onPress={() => {
            navigation.replace('OnBoarding');
          }}
        />
      </View>
    </View>
  );
};

export default Language;

const styles = StyleSheet.create({
  image: {
    width: wp(13), // 54,
    height: hp(10), // 54,
    resizeMode: 'contain',
    marginTop: hp(5),
    marginHorizontal: 20,
  },
  text: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(2.75),
    fontWeight: '500',
    color: '#000000',
    marginHorizontal: 20,
    marginTop: 10,
  },
  languageViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    borderBottomWidth: 0.5,
    borderBottomColor: '#0000004d',
    paddingHorizontal: 15,
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: hp(1.9),
    fontWeight: '400',
    color: '#0000004d',
    marginHorizontal: 20,
    marginTop: 10,
  },
  subimage: {
    height: hp(4.3),
    width: wp(8.7),
    resizeMode: 'contain',
  },
  texted: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(2.5),
    fontWeight: 'bold',
    color: '#1E1F20',
    marginLeft: hp(2),
  },
  tick: {
    height: 20,
    width: 20,
  },
});
