import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {HeaderLight} from '../Custom/CustomView';
import stringsoflanguages from '../language';

const Details = ({navigation}) => {
  const {_callList, _details} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} title={_callList.name} />
      <View
        style={{
          padding: 10,
          marginHorizontal: 20,
          backgroundColor: '#fff',
          elevation: 5,
          marginTop: -70,
          borderRadius: 10,
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.imagebox}
            source={require('../images/photo.png')}
          />
          <Text style={styles.inputtxt}>{_callList.name}</Text>
        </View>
        <Image
          style={styles.sub3image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub4image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub5image}
          source={require('../images/star.png')}
        />
        <Text style={styles.subtext}>{_callList.exp}</Text>
        <Text style={styles.subtext}>
          {_callList.daily} <Text style={{color: '#6CBDFF'}}>₹700</Text>
        </Text>
        <Text style={styles.subtext}>
          {_callList.hourlyRate} <Text style={{color: '#6CBDFF'}}>₹300</Text>
        </Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>{_callList.call}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('Details')}>
            <Text style={styles.touchtext}>{_callList.book}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <Text style={styles.servicetext}>{_details.services}</Text>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.basin}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.bath}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.bath}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.toilet}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.mixer}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.water}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.tick} source={require('../images/tick3.png')} />
          <Text style={styles.title}>{_details.motor}</Text>
        </View>
        <Text style={styles.servicetext}>{_details.about}</Text>
        <Text style={styles.subservicetext}>{_details.loremtext}</Text>
        <Text style={styles.read}>{_details.read}</Text>
        <Text style={styles.servicetext}>{_details.photos}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
          }}>
          <Image
            style={styles.imagebox2}
            source={require('../images/photo.png')}
          />
          <Image
            style={styles.imagebox2}
            source={require('../images/photo.png')}
          />
          <Image
            style={styles.imagebox2}
            source={require('../images/photo.png')}
          />
          <Image
            style={styles.imagebox2}
            source={require('../images/photo.png')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Details;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 203,
    backgroundColor: '#6CBDFF',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  imagebox: {
    height: 100,
    width: 100,
  },
  inputtxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#080040',
    marginHorizontal: 10,
  },
  sub3image: {
    marginHorizontal: 110,
    marginTop: -77,
    height: 14,
    width: 14,
  },
  sub4image: {
    marginHorizontal: 130,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub5image: {
    marginHorizontal: 150,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
    marginHorizontal: 110,
    marginTop: 5,
  },
  touch: {
    width: 150,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6DD400',
    marginTop: 20,
    // marginBottom: 10,
    // alignSelf: 'center',
  },
  touch2: {
    width: 150,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 10,
    // alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
  servicetext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#454545',
    marginTop: 20,
    marginHorizontal: 20,
  },
  tick: {
    width: 17,
    height: 17,
    marginTop: 20,
    marginHorizontal: 20,
  },
  title: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '600',
    color: '#454545',
    marginTop: 17,
    marginLeft: -10,
  },
  subservicetext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 13,
    fontWeight: '400',
    color: '#4545454d',
    marginTop: 10,
    lineHeight: 20,
    marginHorizontal: 20,
  },
  read: {
    color: '#6CBDFF',
    marginTop: 10,
    marginHorizontal: 20,
  },
  imagebox2: {
    width: 100,
    height: 100,
    marginTop: 10,
    marginLeft: 10,
  },
});
