import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import stringsoflanguages from '../language';
import Dash from 'react-native-dash';

const OpenJobs = () => {
  const {_xyzJob} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <ScrollView>
        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.imagebox}
                  source={require('../images/pic.png')}
                />
                <Text style={styles.inputtxt}>{_xyzJob.constName}</Text>
              </View>
              <Text style={styles.subtext}>3.0</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>
            <Image
              style={{
                width: 25,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: 10,
              }}
              source={require('../images/premium.png')}
            />
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.date}>{_xyzJob.expectedPrice}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
            <View>
              <Text style={styles.date}>{_xyzJob.jobSuccess}</Text>
              <Text style={styles.subDate}>100%</Text>
            </View>

            <View>
              <Text style={styles.date}>{_xyzJob.Exp}</Text>
              <Text style={styles.subDate}>10 yrs</Text>
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Image
                style={{width: 20, height: 20, alignSelf: 'center'}}
                source={require('../images/chat.png')}
              />
              <Text style={styles.chat}>{_xyzJob.CHAT}</Text>
            </View>
            <View>
              <Image
                style={{width: 20, height: 20, alignSelf: 'center'}}
                source={require('../images/phone.png')}
              />
              <Text style={styles.chat}>{_xyzJob.CALL}</Text>
            </View>
            <View>
              <Image
                style={{width: 22, height: 20, alignSelf: 'center'}}
                source={require('../images/heart.png')}
              />
              <Text style={styles.chat}>{_xyzJob.SHORTLIST}</Text>
            </View>
            <View>
              <Image
                style={{width: 17, height: 20, alignSelf: 'center'}}
                source={require('../images/medal.png')}
              />
              <Text style={styles.chat}>{_xyzJob.AWARDED}</Text>
            </View>
          </View>
        </View>

        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.imagebox}
                  source={require('../images/pic.png')}
                />
                <Text style={styles.inputtxt}>{_xyzJob.constName}</Text>
              </View>
              <Text style={styles.subtext}>3.0</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.date}>{_xyzJob.expectedPrice}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
            <View>
              <Text style={styles.date}>{_xyzJob.jobSuccess}</Text>
              <Text style={styles.subDate}>100%</Text>
            </View>

            <View>
              <Text style={styles.date}>{_xyzJob.Exp}</Text>
              <Text style={styles.subDate}>10 yrs</Text>
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Image
                style={{width: 20, height: 20, alignSelf: 'center'}}
                source={require('../images/chat.png')}
              />
              <Text style={styles.chat}>{_xyzJob.CHAT}</Text>
            </View>
            <View>
              <Image
                style={{width: 20, height: 20, alignSelf: 'center'}}
                source={require('../images/phone.png')}
              />
              <Text style={styles.chat}>{_xyzJob.CALL}</Text>
            </View>
            <View>
              <Image
                style={{width: 22, height: 20, alignSelf: 'center'}}
                source={require('../images/heart.png')}
              />
              <Text style={styles.chat}>{_xyzJob.SHORTLIST}</Text>
            </View>
            <View>
              <Image
                style={{width: 17, height: 20, alignSelf: 'center'}}
                source={require('../images/medal.png')}
              />
              <Text style={styles.chat}>{_xyzJob.AWARDED}</Text>
            </View>
          </View>
        </View>
        <View style={styles.container}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.imagebox}
                  source={require('../images/pic.png')}
                />
                <Text style={styles.inputtxt}>{_xyzJob.constName}</Text>
              </View>
              <Text style={styles.subtext}>3.0</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.date}>{_xyzJob.expectedPrice}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
            <View>
              <Text style={styles.date}>{_xyzJob.jobSuccess}</Text>
              <Text style={styles.subDate}>100%</Text>
            </View>

            <View>
              <Text style={styles.date}>{_xyzJob.Exp}</Text>
              <Text style={styles.subDate}>10 yrs</Text>
            </View>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Image
                style={{width: 20, height: 20, alignSelf: 'center'}}
                source={require('../images/chat.png')}
              />
              <Text style={styles.chat}>{_xyzJob.CHAT}</Text>
            </View>
            <View>
              <Image
                style={{width: 20, height: 20, alignSelf: 'center'}}
                source={require('../images/phone.png')}
              />
              <Text style={styles.chat}>{_xyzJob.CALL}</Text>
            </View>
            <View>
              <Image
                style={{width: 22, height: 20, alignSelf: 'center'}}
                source={require('../images/heart.png')}
              />
              <Text style={styles.chat}>{_xyzJob.SHORTLIST}</Text>
            </View>
            <View>
              <Image
                style={{width: 17, height: 20, alignSelf: 'center'}}
                source={require('../images/medal.png')}
              />
              <Text style={styles.chat}>{_xyzJob.AWARDED}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const AwardedJobs = () => {
  const {_xyzJob} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.imagebox}
                source={require('../images/pic.png')}
              />
              <Text style={styles.inputtxt}>{_xyzJob.constName}</Text>
            </View>
            <Text style={styles.subtext}>3.0</Text>
            <Image
              style={styles.sub3image}
              source={require('../images/star.png')}
            />
            <Image
              style={styles.sub4image}
              source={require('../images/star.png')}
            />
            <Image
              style={styles.sub5image}
              source={require('../images/star.png')}
            />
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.date}>{_xyzJob.expectedPrice}</Text>
            <Text style={styles.subDate}>₹ 5000</Text>
          </View>
          <View>
            <Text style={styles.date}>{_xyzJob.jobSuccess}</Text>
            <Text style={styles.subDate}>100%</Text>
          </View>

          <View>
            <Text style={styles.date}>{_xyzJob.Exp}</Text>
            <Text style={styles.subDate}>10 yrs</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Image
              style={{width: 20, height: 20, alignSelf: 'center'}}
              source={require('../images/chat.png')}
            />
            <Text style={styles.chat}>{_xyzJob.CHAT}</Text>
          </View>
          <View>
            <Image
              style={{width: 20, height: 20, alignSelf: 'center'}}
              source={require('../images/phone.png')}
            />
            <Text style={styles.chat}>{_xyzJob.CALL}</Text>
          </View>
          <View>
            <Image
              style={{width: 22, height: 20, alignSelf: 'center'}}
              source={require('../images/heart.png')}
            />
            <Text style={styles.chat}>{_xyzJob.SHORTLIST}</Text>
          </View>
          <View>
            <Image
              style={{width: 17, height: 20, alignSelf: 'center'}}
              source={require('../images/medal.png')}
            />
            <Text style={styles.chat}>{_xyzJob.AWARDED}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const CancelledJobs = () => {
  const {_xyzJob} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.imagebox}
                source={require('../images/pic.png')}
              />
              <Text style={styles.inputtxt}>{_xyzJob.constName}</Text>
            </View>
            <Text style={styles.subtext}>3.0</Text>
            <Image
              style={styles.sub3image}
              source={require('../images/star.png')}
            />
            <Image
              style={styles.sub4image}
              source={require('../images/star.png')}
            />
            <Image
              style={styles.sub5image}
              source={require('../images/star.png')}
            />
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.date}>{_xyzJob.expectedPrice}</Text>
            <Text style={styles.subDate}>₹ 5000</Text>
          </View>
          <View>
            <Text style={styles.date}>{_xyzJob.jobSuccess}</Text>
            <Text style={styles.subDate}>100%</Text>
          </View>

          <View>
            <Text style={styles.date}>{_xyzJob.Exp}</Text>
            <Text style={styles.subDate}>10 yrs</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Image
              style={{width: 20, height: 20, alignSelf: 'center'}}
              source={require('../images/chat.png')}
            />
            <Text style={styles.chat}>{_xyzJob.CHAT}</Text>
          </View>
          <View>
            <Image
              style={{width: 20, height: 20, alignSelf: 'center'}}
              source={require('../images/phone.png')}
            />
            <Text style={styles.chat}>{_xyzJob.CALL}</Text>
          </View>
          <View>
            <Image
              style={{width: 22, height: 20, alignSelf: 'center'}}
              source={require('../images/heart.png')}
            />
            <Text style={styles.chat}>{_xyzJob.SHORTLIST}</Text>
          </View>
          <View>
            <Image
              style={{width: 17, height: 20, alignSelf: 'center'}}
              source={require('../images/medal.png')}
            />
            <Text style={styles.chat}>{_xyzJob.AWARDED}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const CompletedJobs = () => {
  const {_xyzJob} = stringsoflanguages;
  return (
    <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.imagebox}
                source={require('../images/pic.png')}
              />
              <Text style={styles.inputtxt}>{_xyzJob.constName}</Text>
            </View>
            <Text style={styles.subtext}>3.0</Text>
            <Image
              style={styles.sub3image}
              source={require('../images/star.png')}
            />
            <Image
              style={styles.sub4image}
              source={require('../images/star.png')}
            />
            <Image
              style={styles.sub5image}
              source={require('../images/star.png')}
            />
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.date}>{_xyzJob.expectedPrice}</Text>
            <Text style={styles.subDate}>₹ 5000</Text>
          </View>
          <View>
            <Text style={styles.date}>{_xyzJob.jobSuccess}</Text>
            <Text style={styles.subDate}>100%</Text>
          </View>

          <View>
            <Text style={styles.date}>{_xyzJob.Exp}</Text>
            <Text style={styles.subDate}>10 yrs</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Image
              style={{width: 20, height: 20, alignSelf: 'center'}}
              source={require('../images/chat.png')}
            />
            <Text style={styles.chat}>{_xyzJob.CHAT}</Text>
          </View>
          <View>
            <Image
              style={{width: 20, height: 20, alignSelf: 'center'}}
              source={require('../images/phone.png')}
            />
            <Text style={styles.chat}>{_xyzJob.CALL}</Text>
          </View>
          <View>
            <Image
              style={{width: 22, height: 20, alignSelf: 'center'}}
              source={require('../images/heart.png')}
            />
            <Text style={styles.chat}>{_xyzJob.SHORTLIST}</Text>
          </View>
          <View>
            <Image
              style={{width: 17, height: 20, alignSelf: 'center'}}
              source={require('../images/medal.png')}
            />
            <Text style={styles.chat}>{_xyzJob.AWARDED}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const renderScene = SceneMap({
  first: OpenJobs,
  second: AwardedJobs,
  third: CancelledJobs,
  fourth: CompletedJobs,
});

const MyJobs = ({navigation}) => {
  const {_xyzJob} = stringsoflanguages;
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: _xyzJob.Applicants},
    {key: 'second', title: _xyzJob.Shortlisted},
    {key: 'third', title: _xyzJob.AWARDED},
    {key: 'fourth', title: _xyzJob.Archieved},
  ]);
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={_xyzJob.title} />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            tabStyle={{}}
            scrollEnabled={false}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default MyJobs;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 5,
    marginTop: 20,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 10,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    marginHorizontal: 10,
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginHorizontal: 20,
    marginTop: 5,
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    marginHorizontal: 20,
    marginTop: 5,
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 10,
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 50,
    marginTop: 20,
  },
  imagebox: {
    height: 50,
    width: 50,
  },
  inputtxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#080040',
    marginHorizontal: 10,
  },
  sub3image: {
    marginHorizontal: 85,
    // marginTop: 20,
    height: 14,
    width: 14,
    marginTop: -16,
  },
  sub4image: {
    marginHorizontal: 100,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub5image: {
    marginHorizontal: 115,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 13,
    fontWeight: '400',
    color: '#454545',
    marginHorizontal: 60,
    marginTop: -25,
  },
  chat: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 10,
    fontWeight: 'bold',
    marginTop: 5,
  },
});
const DashLine = props => (
  <View style={{margin: 15}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
