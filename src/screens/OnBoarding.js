import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import Swiper from 'react-native-swiper';
import {BottomView, ButtonStyle} from '../Custom/CustomView';
import stringsoflanguages from '../language';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
const {height} = Dimensions.get('window');

const data = [
  {
    source: require('../images/htw.png'),
    title: 'What is Lorem Ipsum?',
    titleHeader:
      'Lorem Ipsum is simply dummy text of the\n printing and typesetting industry.',
  },
  {
    source: require('../images/htw.png'),
    title: 'What is Lorem Ipsum?',
    titleHeader:
      'Lorem Ipsum is simply dummy text of the\n printing and typesetting industry.',
  },
  {
    source: require('../images/htw.png'),
    title: 'What is Lorem Ipsum?',
    titleHeader:
      'Lorem Ipsum is simply dummy text of the\n printing and typesetting industry.',
  },
];

const OnBoarding = ({navigation}) => {
  const {_onboarding} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark bg={'#FFFFFF'} />
      <Swiper
        onMomentumScrollEnd={(e, state, context) => {
          // console.log('index:', state.index)
        }}
        // ref="swiper"
        showsButtons={false}
        loop={false}
        dot={<View style={styles.dot} />}
        activeDot={<View style={styles.activeDot} />}
        paginationStyle={{resizeMode: 'contain', marginBottom: hp(2)}}
        showsPagination={true}>
        {data.map(item => {
          const {source, title, titleHeader} = item;
          return (
            <View
              key={'key'}
              style={{
                height: hp(100),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{height: hp(31), width: wp(100)}}>
                <Image style={styles.sub2image} source={source} />
              </View>
              <Text style={styles.titletxt}>{_onboarding.title}</Text>
              <Text style={styles.titleHeadertext}>{_onboarding.subtitle}</Text>
            </View>
          );
        })}
      </Swiper>
      <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
        <View style={{width: '45%'}}>
          <ButtonStyle
            title={_onboarding.skip}
            bgColor={'#49535B'}
            onPress={() => {}}
          />
        </View>
        <View style={{width: '45%'}}>
          <ButtonStyle
            title={_onboarding.next}
            onPress={() => {
              navigation.navigate('Login');
            }}
          />
        </View>
      </View>
      <BottomView />
    </View>
  );
};

export default OnBoarding;

const styles = StyleSheet.create({
  sub2image: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
  },
  dot: {
    backgroundColor: '#D8D8D8',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  activeDot: {
    backgroundColor: '#F5B04C',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4,
    marginTop: 3,
    marginBottom: 3,
  },
  titletxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: hp(3.3),
    fontWeight: '900',
    color: '#00689F',
    textAlign: 'center',
    marginTop: hp(9),
  },
  titleHeadertext: {
    fontFamily: 'Avenir-Medium',
    fontSize: hp(1.9),
    fontWeight: '500',
    color: '#ADADAD',
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 20,
  },
});
