import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import stringsoflanguages from '../language';
const {height} = Dimensions.get('window');

const CallList = ({navigation}) => {
  const {_callList} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={_callList.title} />
      <ScrollView>
        <View style={styles.container}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.text}>{_callList.callRemaining}</Text>
            <Text style={styles.text}>{_callList.calls}</Text>
          </View>
        </View>
        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.imagebox}
              source={require('../images/photo.png')}
            />
            <Text style={styles.inputtxt}>{_callList.name}</Text>
          </View>
          <Image
            style={styles.sub3image}
            source={require('../images/star.png')}
          />
          <Image
            style={styles.sub4image}
            source={require('../images/star.png')}
          />
          <Image
            style={styles.sub5image}
            source={require('../images/star.png')}
          />
          <Text style={styles.subtext}>{_callList.exp}</Text>
          <Text style={styles.subtext}>
            {_callList.daily} <Text style={{color: '#6CBDFF'}}>₹700</Text>
          </Text>
          <Text style={styles.subtext}>
            {_callList.hourlyRate} <Text style={{color: '#6CBDFF'}}>₹300</Text>
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity style={styles.touch}>
              <Text style={styles.touchtext}>{_callList.call}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.touch2}
              onPress={() => navigation.navigate('Success')}>
              <Text style={styles.touchtext}>{_callList.book}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.imagebox}
              source={require('../images/photo.png')}
            />
            <Text style={styles.inputtxt}>{_callList.name}</Text>
          </View>
          <Image
            style={styles.sub3image}
            source={require('../images/star.png')}
          />
          <Image
            style={styles.sub4image}
            source={require('../images/star.png')}
          />
          <Image
            style={styles.sub5image}
            source={require('../images/star.png')}
          />
          <Text style={styles.subtext}>{_callList.exp}</Text>
          <Text style={styles.subtext}>
            {_callList.daily} <Text style={{color: '#6CBDFF'}}>₹700</Text>
          </Text>
          <Text style={styles.subtext}>
            {_callList.hourlyRate} <Text style={{color: '#6CBDFF'}}>₹300</Text>
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity style={styles.touch}>
              <Text style={styles.touchtext}>{_callList.call}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.touch2}
              onPress={() => navigation.navigate('Details')}>
              <Text style={styles.touchtext}>{_callList.book}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              style={styles.imagebox}
              source={require('../images/photo.png')}
            />
            <Text style={styles.inputtxt}>{_callList.name}</Text>
          </View>
          <Image
            style={styles.sub3image}
            source={require('../images/star.png')}
          />
          <Image
            style={styles.sub4image}
            source={require('../images/star.png')}
          />
          <Image
            style={styles.sub5image}
            source={require('../images/star.png')}
          />
          <Text style={styles.subtext}>{_callList.exp}</Text>
          <Text style={styles.subtext}>
            {_callList.daily} <Text style={{color: '#6CBDFF'}}>₹700</Text>
          </Text>
          <Text style={styles.subtext}>
            {_callList.hourlyRate} <Text style={{color: '#6CBDFF'}}>₹300</Text>
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity style={styles.touch}>
              <Text style={styles.touchtext}>{_callList.call}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.touch2}
              onPress={() => navigation.navigate('Success')}>
              <Text style={styles.touchtext}>{_callList.book}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Image
          style={{width: 141, height: 70, marginLeft: 'auto', marginTop: 20}}
          source={require('../images/support.png')}
        />
      </ScrollView>
    </View>
  );
};

export default CallList;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: '100%',
    backgroundColor: '#F2AD4B',
    // marginTop: 10,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  imagebox: {
    // marginTop: 40,
    // marginHorizontal: 20,
    height: 100,
    width: 100,
  },
  inputtxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#080040',
    marginHorizontal: 10,
  },
  sub3image: {
    marginHorizontal: 110,
    marginTop: -77,
    height: 14,
    width: 14,
  },
  sub4image: {
    marginHorizontal: 130,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub5image: {
    marginHorizontal: 150,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
    marginHorizontal: 110,
    marginTop: 5,
  },
  touch: {
    paddingHorizontal: 50,
    borderRadius: 25,
    backgroundColor: '#6DD400',
    marginTop: 20,
    alignSelf: 'center',
  },
  touch2: {
    paddingHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
    marginBottom: 7,
  },
});
