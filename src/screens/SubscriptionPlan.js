import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import stringsoflanguages from '../language';
const {height} = Dimensions.get('window');

const SubscriptionPlan = ({navigation}) => {
  const {_SubscriptionPlanContractor} = stringsoflanguages;
  const [data, setData] = useState([
    {
      id: '1',
      title: _SubscriptionPlanContractor.monthPlan,
      subtitle: '₹5000',
    },
    {
      id: '2',
      title: _SubscriptionPlanContractor.month1Plan,
      subtitle: '₹1000',
    },
    {
      id: '3',
      title: _SubscriptionPlanContractor.month2Plan,
      subtitle: '₹15000',
    },
  ]);
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_SubscriptionPlanContractor.title}
      />
      <ScrollView>
        <FlatList
          numColumns={1}
          data={data}
          renderItem={({item, index}) => (
            <SafeAreaView style={styles.plan}>
              <View style={styles.subplan}>
                <Text style={styles.plantext}>{item.title}</Text>
                <Text style={styles.subplantext}>{item.subtitle}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.tickimg}
                  source={require('../images/tick.png')}
                />
                <Text style={styles.ticktext}>
                  {_SubscriptionPlanContractor.loremTitle}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.tickimg}
                  source={require('../images/tick.png')}
                />
                <Text style={styles.ticktext}>
                  {_SubscriptionPlanContractor.loremSubtitle}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.tickimg}
                  source={require('../images/tick.png')}
                />
                <Text style={styles.ticktext}>
                  {_SubscriptionPlanContractor.loremTitle}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.touch}
                onPress={() => navigation.navigate('Payment')}>
                <Text style={styles.touchtext}>
                  {_SubscriptionPlanContractor.buy}
                </Text>
              </TouchableOpacity>
            </SafeAreaView>
          )}
        />
      </ScrollView>
    </View>
  );
};

export default SubscriptionPlan;

const styles = StyleSheet.create({
  plan: {
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 20,
    marginBottom: 10,
    margin: 20,
  },
  subplan: {
    backgroundColor: '#ECEFF1',
    borderTopEndRadius: 10,
    borderTopLeftRadius: 10,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 30,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  tickimg: {
    width: 17,
    height: 17,
    marginHorizontal: 20,
    marginTop: 10,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 13,
    color: '#8A8A8A',
    marginTop: 8,
    marginLeft: -5,
  },
  touch: {
    width: 141,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 10,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
});
