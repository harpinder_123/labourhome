import CheckBox from '@react-native-community/checkbox';
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import stringsoflanguages from '../../language';
import {LocalStorage, Api} from '../../services/Api';
import {_SetAuthToken} from '../../services/ApiSauce';
import DeviceInfo from 'react-native-device-info';
import {useDispatch, useSelector} from 'react-redux';
import store from '../../redux/store';
import * as EmailValidator from 'email-validator';

let data = [
  {
    id: '1',
    value: 'I am a worker',
  },
  {
    id: '2',
    value: 'I am a contractor',
  },
  {
    id: '3',
    value: 'I am a customer',
  },
];
let workerData = [
  {
    id: '1',
    value: '1-9',
  },
  {
    id: '2',
    value: '10-30',
  },
  {
    id: '3',
    value: '31-99',
  },
];

const RegisterContractor = ({navigation, route}) => {
  const {phone} = useSelector(store => store);
  const [slectTradeText, setSelectTradeText] = useState('');
  const [slectUserTypeText, setSelectUserTypeText] = useState('');
  const [state, setState] = useState({
    mobile: phone,
    name: '',
    company_name: '',
    gstin_no: '',
    adhar_number: '',
    pan_no: '',
    email: '',
    address: '',
    city: '',
    pincode: '',
    additional_information: '',
    term: false,
  });
  const registerHandlerPress = async () => {
    const {
      mobile,
      term,
      name,
      company_name,
      gstin_no,
      adhar_number,
      pan_no,
      email,
      address,
      city,
      pincode,
      additional_information,
    } = state;

    if (name == '') {
      alert('Please ! Enter Your Name');
      return;
    }
    // if (project_type == '') {
    //   alert('Please ! Select User Type');
    //   return;
    // }
    // if (select_trade == '') {
    //   alert('Please ! Select Trade');
    //   return;
    // }
    // if (no_of_worker == '') {
    //   alert('Please ! Select Number of Workers');
    //   return;
    // }
    if (adhar_number == '') {
      alert('Please ! Enter Adhar Number');
      return;
    }
    if (email.length != 0) {
      if (EmailValidator.validate(email) == false) {
        alert('Please Enter Valid Email');
        return;
      }
    }
    if (term == false) {
      alert('Please ! Check Term and Condition');
      return;
    }

    const body = {
      country_code: '+91',
      user_type: 2,
      mobile: mobile,
      name: name,
      company_name: company_name,
      gstin_no: gstin_no,
      adhar_number: adhar_number,
      pan_no: pan_no,
      address: 'ABACASDF',
      city: city,
      pincode: pincode,
      additional_information: additional_information,
      device_id: DeviceInfo.getUniqueId(),
      device_token: 'device_token',
      device_type: DeviceInfo.getDeviceType(),
      loginTime: 'loginTime',
      model_name: 'modelName',
      carrier_name: DeviceInfo.getDeviceType(),
      device_country: 'device_country',
      device_memory: 'device_memory',
      have_notch: 'have_notch',
      manufacture: 'manufacture',
    };

    const response = await Api.ContractorRegisterApi(body);
    //  alert(JSON.stringify(response, null, 2));
    //   console.log(JSON.stringify(response, null, 2));
    const {status = false, token, user_detail, trade = []} = response;
    if (status) {
      _SetAuthToken(token);
      LocalStorage.setToken(token);
      LocalStorage.setUserTypes(user_detail.user_type);
      navigation.replace('HomeContractor');
    } else {
      alert('Something went wrong');
    }
  };
  useEffect(() => {
    // alert(JSON.stringify(route.params));
  }, []);
  useEffect(() => {
    const willFocusSubscription = navigation.addListener('focus', () => {
      LocalStorage.getSelectUserType('selectUserType').then(selectUserType => {
        setSelectUserTypeText(selectUserType);
      });
    });
    return willFocusSubscription;
  }, [navigation]);

  useEffect(() => {
    const willFocusSubscription = navigation.addListener('focus', () => {
      LocalStorage.getSelectTrade('selectTrade').then(selectTrade => {
        setSelectTradeText(selectTrade);
      });
    });
    return willFocusSubscription;
  }, [navigation]);

  const {_ragister} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark bg={'#F4F4F4'} />
      <TextInput
        style={styles.textinput}
        placeholder="Name*"
        onChangeText={name => setState({...state, name})}
        value={state.name}
      />
      <TextInput
        style={styles.textinput}
        placeholder="Company Name"
        onChangeText={company_name => setState({...state, company_name})}
        value={state.company_name}
      />
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('SelectUserTypeContractor', {
            selectType: slectUserTypeText,
          });
        }}
        style={styles.RowView}>
        {slectUserTypeText ? (
          <>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.mndtryText}>{slectUserTypeText}</Text>
            </View>
            <Image
              style={styles.imageData}
              source={require('../../images/arrow.png')}
            />
          </>
        ) : (
          <>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.mndtryText}>{_ragister.selectUserType}</Text>
              <Text style={styles.mndtryTextRed}>*</Text>
            </View>
            <Image
              style={styles.imageData}
              source={require('../../images/arrow.png')}
            />
          </>
        )}
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          navigation.navigate('SelectTradeContractor', {
            selectType: slectTradeText,
          });
        }}
        style={styles.RowView}>
        {slectTradeText ? (
          <>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.mndtryText}>{slectTradeText}</Text>
            </View>
            <Image
              style={styles.imageData}
              source={require('../../images/arrow.png')}
            />
          </>
        ) : (
          <>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.mndtryText}>{_ragister.selectTrade}</Text>
              <Text style={styles.mndtryTextRed}>*</Text>
            </View>
            <Image
              style={styles.imageData}
              source={require('../../images/arrow.png')}
            />
          </>
        )}
      </TouchableOpacity>

      <Dropdown
        style={styles.drops}
        underlineColor="transparent"
        label="Number of Workers*"
        //  icon="cheveron-down"
        iconColor="#0000004d"
        pickerStyle={{width: '88%', left: '6%', marginTop: 20}}
        baseColor={'#FFF'}
        //  onValueChange={(index, indexmove) => navigateUser(index, indexmove)}
        icon={require('../../images/drop.png')}
        data={workerData}
      />
      <TextInput
        style={styles.textinput}
        placeholder="GSTIN Number"
        onChangeText={gstin_no => setState({...state, gstin_no})}
        value={state.gstin_no}
      />
      <TextInput
        style={styles.textinput}
        placeholder="Aadhaar Card Number*"
        onChangeText={adhar_number => setState({...state, adhar_number})}
        value={state.adhar_number}
      />
      <TextInput
        style={styles.textinput}
        placeholder="PAN Number"
        onChangeText={pan_no => setState({...state, pan_no})}
        value={state.pan_no}
      />
      <Text style={styles.blackText}>Official Address</Text>
      <TextInput
        style={styles.textinput}
        placeholder="Email"
        onChangeText={email => setState({...state, email})}
        value={state.email}
      />
      <TextInput
        style={styles.textinput}
        placeholder="Address Line 1"
        onChangeText={address => setState({...state, address})}
        value={state.address}
      />
      <TextInput
        style={styles.textinput}
        placeholder="City"
        onChangeText={city => setState({...state, city})}
        value={state.city}
      />
      <TextInput
        style={styles.textinput}
        placeholder="Pincode"
        onChangeText={pincode => setState({...state, pincode})}
        value={state.pincode}
      />

      <View style={styles.adnlInfotextinput}>
        <TextInput
          style={styles.inputTextData}
          multiline={true}
          placeholder="Additional information"
          onChangeText={additional_information =>
            setState({...state, additional_information})
          }
          value={state.additional_information}
        />
      </View>

      <View style={{flexDirection: 'row', width: wp(90), marginVertical: 10}}>
        <View style={{width: wp(8)}}>
          <CheckBox
            disabled={false}
            tintColors={{true: '#F5B04C', false: 'grey'}}
            value={state.term}
            onValueChange={term => setState({...state, term})}
          />
        </View>
        <View style={{width: wp(82)}}>
          <Text style={[styles.agreekatlegoOffCss, {paddingVertical: 3}]}>
            {_ragister.bySigning}
            <Text style={styles.privacyOffCss}>
              {' '}
              {_ragister.termCondition}{' '}
            </Text>
            <Text style={styles.agreekatlegoOffCss}>{_ragister.and}</Text>
            <Text style={styles.privacyOffCss}> {_ragister.privacyPolicy}</Text>
          </Text>
        </View>
      </View>
      <TouchableOpacity
        style={styles.touch}
        // onPress={() => navigation.navigate('HomeContractor')}
        onPress={() => registerHandlerPress()}>
        <Text style={styles.touchtext}>{_ragister.SUBMIT}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default RegisterContractor;

const styles = StyleSheet.create({
  inputTextData: {
    marginBottom: 5,
    marginHorizontal: 10,
  },
  adnlInfotextinput: {
    height: 120,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    // marginTop: 25,
    marginVertical: 10,
    marginHorizontal: 0,
  },
  blackText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: 'bold',
    fontSize: 16,
    // marginTop: 22,
    color: '#454545',
    // marginBottom: -10,
  },
  imageData: {height: 15, width: 10, marginRight: 20, marginTop: 20},
  mndtryText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 19,
    color: '#454545',
    marginLeft: 20,
  },
  mndtryTextRed: {
    fontFamily: 'Avenir-Medium',
    fontWeight: 'bold',
    fontSize: 15,
    marginTop: 19,
    color: '#E02020',
  },
  RowView: {
    height: 60,
    borderRadius: 20,
    justifyContent: 'space-between',
    marginTop: 25,
    backgroundColor: '#FFF',
    flexDirection: 'row',
  },
  text: {
    fontFamily: 'Avenir',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#454545',
    marginTop: '30%',
    marginHorizontal: 0,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#606E87',
    marginTop: 10,
    marginHorizontal: 0,
    lineHeight: 20,
  },
  drops: {
    height: 60,
    backgroundColor: '#fff',
    marginHorizontal: 0,
    marginTop: 25,
    borderRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0,
    elevation: 2,
  },
  textinput: {
    height: 60,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginVertical: 10,
    marginHorizontal: 0,
    padding: 20,
  },
  touch: {
    padding: 15,
    marginHorizontal: 0,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 40,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
  agreekatlegoOffCss: {
    color: '#2D2627',
    fontSize: hp(1.75),
    fontFamily: 'Avenir-Meduim',
    paddingHorizontal: 2,
    letterSpacing: 0.5,
  },
  privacyOffCss: {
    color: '#6CBDFF',
    paddingHorizontal: 5,
    fontSize: hp(1.7),
    fontFamily: 'Avenir-Meduim',
    letterSpacing: 0.5,
  },
});
