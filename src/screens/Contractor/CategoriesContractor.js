import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import Dash from 'react-native-dash';
import MaterialTabs from 'react-native-material-tabs';
import stringsoflanguages from '../../language';

const CategoriesContractor = ({navigation}) => {
  const {_categoriesContractor} = stringsoflanguages;
  const [selectedTab, setSelectedTab] = useState(0);
  const [jobList, setJobList] = useState([
    {
      id: '1',
    },
    {
      id: '2',
    },
  ]);
  const [shortlistList, setShortlist] = useState([
    {
      id: '3',
    },
    {
      id: '4',
    },
  ]);

  const categorySelect = index => {
    setSelectedTab(index);
    if (index == 0) {
      setJobList('job');
    } else if (index == 1) {
      setShortlist('shortlist');
    }
  };

  const renderItemMyJob = ({item, index}) => {
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={styles.jobView}>
              <Text style={styles.text}>{_categoriesContractor.xyzJob}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{
                  width: 20,
                  height: 25,
                  marginHorizontal: 10,
                }}
                source={require('../../images/medal.png')}
              />
            </View>
          </View>
          <Text style={styles.timeTextt}>
            {_categoriesContractor.loremtext}
          </Text>

          <DashLine />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_categoriesContractor.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_categoriesContractor.duration}</Text>
              <Text style={styles.subDate}>
                {_categoriesContractor.shortTerm}
              </Text>
            </View>

            <View style={{width: '25%'}}>
              <Text style={styles.date}>{_categoriesContractor.budget}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
          </View>
          <DashLine />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'center',
              width: '90%',
            }}>
            <View style={{width: '48%'}}>
              <Text style={styles.date}>{_categoriesContractor.project}</Text>
              <Text style={styles.subDate}>
                {_categoriesContractor.interior}
              </Text>
            </View>

            <View style={{width: '48%'}}>
              <Text style={styles.date}>
                {_categoriesContractor.jobLocation}
              </Text>
              <Text style={styles.subDate}>
                {_categoriesContractor.address}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              marginTop: 20,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity style={styles.rowDataButtonOrange}>
              <Text style={styles.textShortOrange}>
                {_categoriesContractor.shortlist}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('jobDetailsContractor')}
              style={styles.rowDataButtonBlue}>
              <Text style={styles.textViewDetailsWhite}>
                {_categoriesContractor.viewButton}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  const renderItemCompleted = ({item, index}) => {
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={styles.jobView}>
              <Text style={styles.text}>{_categoriesContractor.xyzJob}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.timeText}>{_categoriesContractor.time}</Text>
              <View
                // onPress={() => navigation.navigate('XyzJob')}
                style={styles.redDaysView}>
                <Text style={styles.textdays}>
                  {_categoriesContractor.days}
                </Text>
              </View>
            </View>
          </View>
          <Text style={styles.timeTextt}>
            {_categoriesContractor.loremtext}
          </Text>

          <DashLine />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_categoriesContractor.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_categoriesContractor.duration}</Text>
              <Text style={styles.subDate}>
                {_categoriesContractor.shortTerm}
              </Text>
            </View>

            <View style={{width: '25%'}}>
              <Text style={styles.date}>{_categoriesContractor.budget}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
          </View>
          <DashLine />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'center',
              width: '90%',
            }}>
            <View style={{width: '48%'}}>
              <Text style={styles.date}>{_categoriesContractor.project}</Text>
              <Text style={styles.subDate}>
                {_categoriesContractor.interior}
              </Text>
            </View>

            <View style={{width: '48%'}}>
              <Text style={styles.date}>
                {_categoriesContractor.jobLocation}
              </Text>
              <Text style={styles.subDate}>
                {_categoriesContractor.address}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              marginTop: 20,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity style={styles.rowDataButton}>
              <Text style={styles.textShortBlack}>
                {_categoriesContractor.shortlist}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('jobDetailsContractor')}
              style={styles.rowDataButtonBlue}>
              <Text style={styles.textViewDetailsWhite}>
                {_categoriesContractor.viewButton}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_categoriesContractor.title}
      />
      <MaterialTabs
        items={[_categoriesContractor.jobs, _categoriesContractor.shortlist]}
        selectedIndex={selectedTab}
        onChange={index => categorySelect(index)}
        barColor="#F2AD4B"
        indicatorColor="#FFF"
        activeTextColor="#FFF"
        inactiveTextColor="#FFF"
        indicatorHeight={4}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        {selectedTab == 0 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={shortlistList}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemCompleted}
          />
        )}
        {selectedTab == 1 && (
          <FlatList
            style={{width: '100%', marginTop: 3}}
            data={jobList}
            showsVerticalScrollIndicator={false}
            renderItem={renderItemMyJob}
          />
        )}
      </ScrollView>
    </View>
  );
};

export default CategoriesContractor;

const styles = StyleSheet.create({
  rowDatabtSpace1: {
    flexDirection: 'row',
    width: '95%',
    alignSelf: 'center',
    justifyContent: 'space-between',
  },
  timeTextt: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginLeft: 12,
    marginTop: 5,
  },
  timeText: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginTop: 8,
  },
  jobView: {
    justifyContent: 'center',
    width: '50%',
  },
  textdays: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
    marginHorizontal: 10,
  },
  redDaysView: {
    justifyContent: 'center',
    backgroundColor: '#E02020',
    borderRadius: 10,
    height: 36,
  },
  textShortBlack: {
    alignSelf: 'center',
    color: '#00000070',
    marginVertical: 7,
  },
  textShortOrange: {
    alignSelf: 'center',
    color: '#F2AD4B',
    marginVertical: 7,
  },
  textViewDetailsWhite: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
  },
  rowDataButtonBlue: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
  },
  rowDataButton: {
    justifyContent: 'center',
    width: '47%',
    borderColor: '#6D7278',
    borderWidth: 1,
    borderRadius: 20,
  },
  rowDataButtonOrange: {
    justifyContent: 'center',
    width: '47%',
    borderColor: '#F2AD4B',
    borderWidth: 1,
    borderRadius: 20,
  },
  container: {
    padding: 10,
    marginHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 5,
    marginVertical: 10,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 15,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
    marginLeft: 15,
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    alignSelf: 'flex-end',
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
