import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  FlatList,
  TextInput,
  Dimensions,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import Dash from 'react-native-dash';
import Dialog, {DialogContent} from 'react-native-popup-dialog';
import {RadioButton} from 'react-native-paper';
import stringsoflanguages from '../../language';

const jobDetailsContractor = ({navigation}) => {
  const {_jobDetailsContractor} = stringsoflanguages;
  const [visible, setVisible] = useState(false);
  const [visibleNegotiation, setVisibleNegotiation] = useState(false);
  const [visibleSuccessfull, setVisibleSuccessfull] = useState(false);
  const [checked, setChecked] = useState('first');
  const [listSubscription, setListSubscription] = useState([
    {
      keys: 5,
    },
    {
      keys: 6,
    },
    {
      keys: 7,
    },
    {
      keys: 8,
    },
  ]);
  function handleBackButtonClick() {
    setVisible(false);
    setVisibleNegotiation(false);
    setVisibleSuccessfull(false);
    //  setVisibleComplete(false);
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  const renderItemSubscriptionPlan = ({item, index}) => {
    return (
      <View style={styles.plan}>
        <View
          style={{
            backgroundColor: '#ECEFF1',
            borderTopEndRadius: 10,
            borderTopLeftRadius: 10,
          }}>
          <View
            style={{
              height: 4,
              width: '100%',
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              backgroundColor: '#F2AD4B',
            }}></View>
          <Text style={styles.plantext}>{_jobDetailsContractor.monthPlan}</Text>
          <Text style={styles.subplantext}>₹300</Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>
            {_jobDetailsContractor.loremTitle}
          </Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>
            {_jobDetailsContractor.loremSubtitle}
          </Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/cross.png')}
          />
          <Text style={styles.ticktext}>
            {_jobDetailsContractor.loremTitle}
          </Text>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('PaymentContractor')}>
          <Text style={styles.touchtext}>{_jobDetailsContractor.buy}</Text>
        </TouchableOpacity>
      </View>
    );
  };
  const handlePaymentSuccessful = () => {
    navigation.navigate('CategoriesContractor');
    setVisible(false);
  };
  const handleVisibleNegotiation = () => {
    //  navigation.navigate('CategoriesContractor');
    setVisibleNegotiation(false);
    setVisibleSuccessfull(true);
  };
  const handleVisibleSuccessfull = () => {
    navigation.replace('HomeContractor');
    setVisibleSuccessfull(false);
  };

  return (
    <View style={{backgroundColor: '#FFF', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_jobDetailsContractor.title}
      />
      <ScrollView>
        <View style={{flex: 1, backgroundColor: '#F4F4F4'}}>
          <View style={styles.container}>
            <View style={styles.jobView}>
              <Text style={styles.text}>
                {_jobDetailsContractor.specification}
              </Text>
            </View>

            <Text style={styles.timeTextt}>
              {_jobDetailsContractor.loremtext}
            </Text>

            <DashLine />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '90%',
                alignSelf: 'center',
              }}>
              <View style={{width: '30%'}}>
                <Text style={styles.date}>
                  {_jobDetailsContractor.startDate}
                </Text>
                <Text style={styles.subDate}>12/12/2021</Text>
              </View>

              <View style={{width: '30%'}}>
                <Text style={styles.date}>
                  {_jobDetailsContractor.duration}
                </Text>
                <Text style={styles.subDate}>
                  {_jobDetailsContractor.shortTerm}
                </Text>
              </View>

              <View style={{width: '25%'}}>
                <Text style={styles.date}>{_jobDetailsContractor.budget}</Text>
                <Text style={styles.subDate}>₹ 5000</Text>
              </View>
            </View>
            <DashLine />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignSelf: 'center',
                width: '90%',
              }}>
              <View style={{width: '48%'}}>
                <Text style={styles.date}>{_jobDetailsContractor.project}</Text>
                <Text style={styles.subDate}>
                  {_jobDetailsContractor.interior}
                </Text>
              </View>

              <View style={{width: '48%'}}>
                <Text style={styles.date}>
                  {_jobDetailsContractor.jobLocation}
                </Text>
                <Text style={styles.subDate}>
                  {_jobDetailsContractor.address}
                </Text>
              </View>
            </View>
            <DashLine />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignSelf: 'center',
                width: '90%',
              }}>
              <View style={{width: '48%'}}>
                <Text style={styles.date}>
                  {_jobDetailsContractor.professional}
                </Text>
                <Text style={styles.subDate}>{_jobDetailsContractor.more}</Text>
              </View>

              <View style={{width: '48%'}}>
                <Text style={styles.date}>{_jobDetailsContractor.area}</Text>
                <Text style={styles.subDate}>
                  {_jobDetailsContractor.square}
                </Text>
              </View>
            </View>
            <DashLine />
            <View style={styles.jobView}>
              <Text style={styles.text}>
                {_jobDetailsContractor.specification}
              </Text>
            </View>

            <Text style={styles.timeTextt}>
              {_jobDetailsContractor.loremtext}
            </Text>
            <DashLine />
            <Text style={styles.textLeft}>{_jobDetailsContractor.site}</Text>
            <View style={styles.rowSecondData}>
              <Image
                style={{
                  width: 90,
                  height: 80,
                  resizeMode: 'contain',
                  borderRadius: 10,
                }}
                source={require('../../images/Group2.png')}
              />
              <ImageBackground
                imageStyle={styles.bgImageStyle}
                source={require('../../images/Group2.png')}>
                <Image
                  style={{height: 35, width: 35, marginLeft: 43, marginTop: 23}}
                  source={require('../../images/playbtn.png')}
                />
              </ImageBackground>
            </View>
          </View>
          <View
            style={{width: '100%', backgroundColor: '#FFF', marginVertical: 5}}>
            <View style={styles.rowSeconddata}>
              <View
                style={{
                  height: 50,
                  width: 50,
                  justifyContent: 'center',
                  borderRadius: 22,
                }}>
                <Image
                  style={styles.imageDataa}
                  source={require('../../images/pic.png')}
                />
              </View>
              <Text style={styles.alreadyText}>
                {_jobDetailsContractor.contractor}
              </Text>
            </View>
          </View>
          <View style={{width: '100%', backgroundColor: '#FFF'}}>
            <Text style={styles.textLeft}>
              {_jobDetailsContractor.bidPremium}
            </Text>
            <View
              style={{
                width: '90%',
                flexDirection: 'row',
                alignSelf: 'center',
                marginBottom: 10,
              }}>
              <View style={styles.radioView}>
                <RadioButton
                  value="first"
                  status={checked === 'first' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('first')}
                  uncheckedColor={'#69707F'}
                  color={'#7BAAED'}
                />
                <Text style={styles.text2_Style}>
                  {_jobDetailsContractor.yes}
                </Text>
              </View>
              <View style={styles.radioView}>
                <RadioButton
                  value="second"
                  status={checked === 'second' ? 'checked' : 'unchecked'}
                  onPress={() => setChecked('second')}
                  uncheckedColor={'#69707F'}
                  color={'#7BAAED'}
                />
                <Text style={styles.text2_Style}>
                  {_jobDetailsContractor.no}
                </Text>
              </View>
            </View>

            <FlatList
              style={{width: '100%', marginTop: 3}}
              data={listSubscription}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={renderItemSubscriptionPlan}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              alignSelf: 'center',
              marginTop: 20,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={() => setVisible(true)}
              style={styles.rowDataButton}>
              <Text style={styles.textViewDetailsWhite}>
                {_jobDetailsContractor.Decline}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={() => navigation.navigate('jobDetailsContractor')}
              onPress={() => setVisibleNegotiation(true)}
              style={styles.rowDataButtonBlue}>
              <Text style={styles.textViewDetailsWhite}>
                {_jobDetailsContractor.Negotiation}
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            // onPress={() => navigation.navigate('jobDetailsContractor')}
            style={styles.touchGreen}>
            <Text style={styles.textViewDetailsWhite}>
              {_jobDetailsContractor.Accept}
            </Text>
          </TouchableOpacity>
        </View>
        <Dialog
          visible={visible}
          onTouchOutside={() => {
            setVisible({visible: true});
          }}>
          <DialogContent>
            <View style={{width: 250, backgroundColor: '#FFF'}}>
              <Image
                style={{
                  height: 70,
                  width: 70,
                  marginTop: 20,
                  alignSelf: 'center',
                }}
                source={require('../../images/crossing.png')}
              />
              <Text style={styles.rejectTextss}>
                {_jobDetailsContractor.sure}
              </Text>
              <Text style={styles.rejectSub}>
                {_jobDetailsContractor.DeclineText}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '90%',
                  alignSelf: 'center',
                  marginTop: 20,
                  justifyContent: 'space-between',
                }}>
                <TouchableOpacity
                  onPress={() => setVisible(false)}
                  setVisible={false}
                  style={styles.rowDataButton}>
                  <Text style={styles.textViewDetailsWhite}>
                    {_jobDetailsContractor.no}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => handlePaymentSuccessful()}
                  setVisible={false}
                  style={styles.rowDataYes}>
                  <Text style={styles.textViewDetailsWhite}>
                    {_jobDetailsContractor.yes}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </DialogContent>
        </Dialog>

        <Dialog
          visible={visibleNegotiation}
          onTouchOutside={() => {
            setVisibleNegotiation({visibleNegotiation: true});
          }}>
          <DialogContent>
            <View style={{width: 250, backgroundColor: '#FFF'}}>
              <Text style={styles.rejectSubleft}>
                {_jobDetailsContractor.Negotiation}
              </Text>
              <TextInput
                style={styles.textinput}
                placeholder={_jobDetailsContractor.price}
              />
              <View style={styles.lineView}></View>
              <TouchableOpacity
                style={styles.touch}
                onPress={() => handleVisibleNegotiation()}
                setVisibleNegotiation={false}
                // onPress={() => navigation.navigate('PaymentContractor')}
              >
                <Text style={styles.touchtext}>
                  {_jobDetailsContractor.send}
                </Text>
              </TouchableOpacity>
            </View>
          </DialogContent>
        </Dialog>

        <Dialog
          visible={visibleSuccessfull}
          onTouchOutside={() => {
            setVisibleSuccessfull({visibleSuccessfull: true});
          }}>
          <DialogContent>
            <View style={{width: 250, backgroundColor: '#FFF'}}>
              <Image
                style={{
                  height: 73,
                  width: 73,
                  marginTop: 20,
                  alignSelf: 'center',
                }}
                source={require('../../images/mark.png')}
              />
              <Text style={styles.rejectSuccess}>
                {_jobDetailsContractor.successful}
              </Text>
              <TouchableOpacity
                onPress={() => handleVisibleSuccessfull()}
                setVisibleSuccessfull={false}
                style={styles.touchii}>
                <Text style={styles.title}>{_jobDetailsContractor.ok}</Text>
              </TouchableOpacity>
            </View>
          </DialogContent>
        </Dialog>
      </ScrollView>
    </View>
  );
};

export default jobDetailsContractor;

const styles = StyleSheet.create({
  rejectSuccess: {
    marginTop: 10,
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: 20,
    fontSize: 18,
    lineHeight: 22,
    fontFamily: 'Avenir-Heavy',
    color: '#000',
    fontWeight: '600',
  },
  lineView: {
    width: '100%',
    alignSelf: 'center',
    height: 1,
    backgroundColor: '#00000050',
  },
  textinput: {
    color: '#000',
  },
  rejectSubleft: {
    marginTop: 10,
    fontSize: 15,
    lineHeight: 18,
    fontFamily: 'Avenir-Medium',
    color: '#00000060',
    fontWeight: '500',
    marginLeft: 4,
  },
  rejectEnterPrice: {
    marginTop: 5,
    fontSize: 15,
    lineHeight: 18,
    fontFamily: 'Avenir-Medium',
    color: '#00000060',
    fontWeight: '500',
  },
  rejectSub: {
    marginTop: 10,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 15,
    lineHeight: 18,
    fontFamily: 'Avenir-Medium',
    color: '#00000060',
    fontWeight: '500',
  },
  rejectTextss: {
    marginTop: 10,
    alignSelf: 'center',
    textAlign: 'center',
    marginHorizontal: 40,
    fontSize: 25,
    lineHeight: 29,
    fontFamily: 'Avenir-Heavy',
    color: '#000',
    fontWeight: '900',
  },
  title: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'Avenir-Heavy',
    textAlign: 'center',
  },
  touchii: {
    alignSelf: 'center',
    backgroundColor: '#6CBDFF',
    width: '45%',
    borderRadius: 12,
    marginTop: 20,
    marginBottom: 10,
    paddingVertical: 6,
  },
  bgImageStyle: {
    width: 90,
    height: 80,
    marginLeft: 15,
    resizeMode: 'contain',
    borderRadius: 10,
  },
  radioView: {
    width: 130,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    color: '#263238',
    fontWeight: 'bold',
    marginLeft: 5,
  },
  subsInternalText: {
    flexDirection: 'row',
    marginTop: 10,
    width: '88%',
    marginHorizontal: 10,
  },
  plan: {
    width: 208,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 5,
    marginHorizontal: 10,
    marginBottom: 4,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
    marginHorizontal: 20,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  tickimg: {
    width: 14,
    height: 14,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    marginTop: -2,
    marginRight: 10,
    marginLeft: 5,
    color: '#8A8A8A',
  },
  touch: {
    width: 100,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },

  imageDataa: {
    width: 50,
    alignSelf: 'center',
    height: 50,
    resizeMode: 'contain',
  },
  rowSeconddata: {
    flexDirection: 'row',
    width: '87%',
    alignSelf: 'center',
    marginVertical: 10,
    marginTop: 10,
  },
  rowSecondData: {
    flexDirection: 'row',
    width: '92%',
    alignSelf: 'center',
    marginTop: 3,
  },
  textLeft: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    fontWeight: '800',
    lineHeight: 18,
    marginLeft: 15,
  },
  alreadyText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    fontWeight: '800',
    lineHeight: 20,
    marginTop: 14,
    marginLeft: 12,
  },
  touchGreen: {
    marginTop: 20,
    marginBottom: 30,
    alignSelf: 'center',
    justifyContent: 'center',
    width: '90%',
    backgroundColor: '#6DD400',
    borderRadius: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  timeTextt: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginLeft: 15,
    lineHeight: 20,
    marginTop: 3,
    textAlign: 'justify',
  },
  timeText: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginTop: 8,
  },
  jobView: {
    alignSelf: 'center',
    width: '92%',
  },
  textdays: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
    marginHorizontal: 10,
  },
  redDaysView: {
    justifyContent: 'center',
    backgroundColor: '#E02020',
    borderRadius: 10,
    height: 36,
  },
  textShortBlack: {
    alignSelf: 'center',
    color: '#6D7278',
    marginVertical: 7,
  },
  textViewDetailsWhite: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 8,
  },
  rowDataButtonBlue: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
  },
  rowDataYes: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#6DD400',
    borderRadius: 20,
  },
  rowDataButton: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#E02020',
    borderRadius: 20,
  },
  container: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
    marginTop: 2,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
