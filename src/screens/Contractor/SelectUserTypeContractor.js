import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AppHeader from '../../Custom/CustomAppHeader';
import {RadioButton} from 'react-native-paper';
import {ButtonStyle} from '../../Custom/CustomView';
import {LocalStorage, Api} from '../../services/Api';

const SelectUserTypeContractor = ({navigation, route}) => {
  const [trade, setTrade] = useState([]);
  const [selectype, setSelectype] = useState(route.params?.selectType);
  const tradeHandlerPress = async () => {
    const response = await Api.getProjectApi({});
    //  alert(JSON.stringify(response, null, 2));
    const {status = false, project = []} = response;
    if (status) {
      setTrade(project);
    } else {
      alert('Something went wrong');
    }
  };
  useEffect(() => {
    tradeHandlerPress();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      console.log('-----slectTradeText : ', selectype);
    });
    return unsubscribe;
  }, [navigation]);

  const selectTypeFunction = item => {
    setSelectype(item.name);
    LocalStorage.setSelectUserType(item.name);
    console.log('-----: ', item.name);
  };

  const onClickNext = () => {
    if (!selectype) {
      alert('Please select your project');
      return;
    } else {
      tradeHandlerPress();
      navigation.navigate('RegisterContractor', {slectTradeText: selectype});
    }
  };

  const jobListFunction = ({item, index}) => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.selectJobNameViewOffCss}
        onPress={() => {
          selectTypeFunction(item);
        }}>
        <Text style={styles.selectJobNameTextOffCss}>{item.name}</Text>
        <RadioButton
          value="second"
          color={'#7BAAED'}
          uncheckedColor={'#69707F'}
          status={selectype === item.name ? 'checked' : 'unchecked'}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.continer}>
      <StatusBarDark bg="#FFF" />
      <AppHeader
        backOnClick={() => {
          navigation.goBack();
        }}
        backIcon={require('../../images/e-remove.png')}
        title={'Project Type'}
        searchOnClick={() => {}}
        search={require('../../images/search.png')}
      />
      {/* <ScrollView nestedScrollEnabled={true} contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}></ScrollView> */}
      <View style={{flex: 1, marginHorizontal: 20}}>
        <FlatList
          data={trade}
          renderItem={jobListFunction}
          showsVerticalScrollIndicator={false}
        />
        <View style={{marginBottom: 20}}>
          {/* <ButtonStyle
            title={'SUBMIT'}
            onPress={() => {
              onClickNext();
            }}
          /> */}
        </View>
      </View>
    </View>
  );
};

export default SelectUserTypeContractor;

const styles = StyleSheet.create({
  continer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    // paddingHorizontal: 20,
  },
  selectJobNameViewOffCss: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomWidth: 0.6,
    borderBottomColor: '#979797',
    alignItems: 'center',
    marginBottom: 5,
  },
  selectJobNameTextOffCss: {
    fontSize: hp(2.07),
    fontFamily: 'Avenir-Medium',
    color: '#333333',
    fontWeight: '500',
  },
});
