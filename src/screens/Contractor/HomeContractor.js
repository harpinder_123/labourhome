import React from 'react';
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Switch,
} from 'react-native';
import {useState} from 'react';
import SwitchToggle from 'react-native-switch-toggle';
import {TouchableRipple} from 'react-native-paper';
import stringsoflanguages from '../../language';

const HomeContractor = ({navigation}) => {
  const {_HomeContractor} = stringsoflanguages;
  const [listdata, setlistData] = useState([
    {
      name: _HomeContractor.jobRequest,
      keys: 1,
      source: require('../../images/gg1.png'),
    },
    {
      name: _HomeContractor.offersMade,
      keys: 2,
      source: require('../../images/gg2.png'),
    },
    {
      name: _HomeContractor.myProjects,
      keys: 3,
      source: require('../../images/gg3.png'),
    },
    {
      name: _HomeContractor.myAccount,
      keys: 4,
      source: require('../../images/gg4.png'),
    },
  ]);
  const [listSubscription, setListSubscription] = useState([
    {
      keys: 5,
    },
    {
      keys: 6,
    },
    {
      keys: 7,
    },
    {
      keys: 8,
    },
  ]);
  const [listVideoTutorial, setListVideoTutorial] = useState([
    {
      keys: 9,
    },
    {
      keys: 10,
    },
    {
      keys: 11,
    },
    {
      keys: 12,
    },
  ]);
  const [isEnabled, setIsEnabled] = useState(false);
  const onPressCategory = (item, index) => {
    const {keys} = item;
    switch (keys) {
      case 1:
        navigation.navigate('CategoriesContractor');
        break;

      case 2:
        navigation.navigate('OfferMadeContractor');
        break;

      case 3:
        navigation.navigate('MyProjectsContractor');
        break;

      case 4:
        navigation.navigate('MyAccountContractor');
        break;
      default:
    }
  };

  const toggleSwitch = () => {
    setIsEnabled(previousState => !previousState);
  };

  const renderItemFourCategories = ({item, index}) => {
    return (
      <TouchableRipple
        rippleColor="rgba(0, 0, 0, .32)"
        onPress={() => onPressCategory(item, index)}
        style={{
          alignSelf: 'center',
          justifyContent: 'center',
          width: '45%',
          marginHorizontal: 8,
          marginVertical: 10,
          alignSelf: 'center',
          elevation: 2,
          backgroundColor: '#fff',
          borderRadius: 8,
        }}>
        <View
          style={{
            width: '80%',
            alignSelf: 'center',
            marginTop: 20,
            borderRadius: 25,
            justifyContent: 'center',
          }}>
          <Image style={{height: 50, width: 50}} source={item.source} />

          <Text style={styles.contracttext}>{item.name}</Text>
        </View>
      </TouchableRipple>
    );
  };

  const renderItemSubscriptionPlan = ({item, index}) => {
    return (
      <View style={styles.plan}>
        <View
          style={{
            backgroundColor: '#ECEFF1',
            borderTopEndRadius: 10,
            borderTopLeftRadius: 10,
          }}>
          <View
            style={{
              height: 4,
              width: '100%',
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10,
              backgroundColor: '#F2AD4B',
            }}></View>
          <Text style={styles.plantext}>{_HomeContractor.monthPlan}</Text>
          <Text style={styles.subplantext}>₹300</Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>{_HomeContractor.loremTitle}</Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>{_HomeContractor.loremSubtitle}</Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/cross.png')}
          />
          <Text style={styles.ticktext}>{_HomeContractor.loremTitle}</Text>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('PaymentContractor')}>
          <Text style={styles.touchtext}>{_HomeContractor.buy}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const renderItemVideoTutorial = ({item, index}) => {
    return (
      <View style={styles.videoView}>
        <View
          style={{
            //  justifyContent: 'center',
            alignSelf: 'center',
            width: 140,
            height: 100,
          }}>
          <ImageBackground
            imageStyle={styles.video}
            source={require('../../images/Group2.png')}>
            <Image
              style={styles.play}
              source={require('../../images/play.png')}
            />
          </ImageBackground>
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
          }}>
          <Text style={styles.playtext}>{_HomeContractor.quotation}</Text>
          <Text style={styles.subplaytext}>{_HomeContractor.min}</Text>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View
        style={{
          marginVertical: 10,
          flexDirection: 'row',
          alignSelf: 'center',
          justifyContent: 'space-between',
          width: '90%',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.image}
            source={require('../../images/headlogo.png')}
          />
          <View
            style={{
              flexDirection: 'column',
              marginLeft: 10,
              width: '75%',
            }}>
            <Text style={styles.text}>{_HomeContractor.title}</Text>
            <Text style={styles.subtext}>{_HomeContractor.subTitle}</Text>
          </View>
        </View>
        <View style={styles.search2}>
          <Image
            style={styles.subimage}
            source={require('../../images/notification.png')}
          />
        </View>
      </View>

      <ScrollView showsVerticalScrollIndicator={false}>
        {/* <Banner /> */}
        <View
          style={{
            marginTop: 10,
            width: '100%',
            height: 50,
            justifyContent: 'center',
            backgroundColor: '#6CBDFF',
          }}>
          <View style={{width: '90%', alignSelf: 'center'}}>
            <Text style={styles.subtext}>{_HomeContractor.xyzJob}</Text>
          </View>
        </View>
        <View
          style={{
            marginTop: 10,
            width: '95%',
            height: 80,
            alignSelf: 'center',
            borderRadius: 10,
            backgroundColor: '#F2AD4B',
          }}>
          <View
            style={{
              marginTop: 10,
              width: '90%',
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{width: '75%'}}>
              <Text style={styles.textWhite}>{_HomeContractor.status}</Text>
            </View>
            <View
              style={{
                flex: 0.4,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Switch
                trackColor={{false: '#767577', true: '#4DD865'}}
                thumbColor={isEnabled ? '#FFF' : '#FFF'}
                ios_backgroundColor="#3e3e3e"
                onValueChange={() => {
                  toggleSwitch();
                }}
                value={isEnabled}
                style={{transform: [{scaleX: 1.2}, {scaleY: 1.2}]}}
              />
            </View>
            {/* <SwitchToggle
              containerStyle={{width: 60, height: 30, borderRadius: 20}}
              circleStyle={{width: 30, height: 29, borderRadius: 15}}
              circleColorOff="#FFF"
              circleColorOn="#FFF"
              backgroundColorOn="#4DD865"
              backgroundColorOff="#4DD865"
              // onPress={() => off(!on)}
            /> */}
          </View>
          <Text style={styles.textWhiteavailable}>
            {_HomeContractor.available}
          </Text>
        </View>
        <FlatList
          style={{width: '100%', marginTop: 3}}
          data={listdata}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          renderItem={renderItemFourCategories}
        />

        <View style={styles.rowPlan}>
          <Text style={styles.sub2text}>{_HomeContractor.plan}</Text>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text style={styles.sub3text}>{_HomeContractor.showAll}</Text>
            <Image
              style={styles.videoimg}
              source={require('../../images/video.png')}
            />
          </View>
        </View>

        <FlatList
          style={{width: '100%', marginTop: 3}}
          data={listSubscription}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItemSubscriptionPlan}
        />
        <Text style={styles.texted}>{_HomeContractor.activePlan}</Text>
        <View style={styles.imageBackView}>
          <ImageBackground
            source={require('../../images/plan.png')}
            imageStyle={styles.month}>
            <Text style={styles.monthtext}>{_HomeContractor.monthPlan}</Text>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{
                  width: 14,
                  height: 14,
                  marginHorizontal: 20,
                  marginTop: 10,
                }}
                source={require('../../images/tick.png')}
              />
              <Text style={styles.submonth}>{_HomeContractor.leads}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginBottom: 15,
              }}>
              <Text style={styles.sub2month}>{_HomeContractor.call}</Text>
              <Text style={styles.sub2month}>{_HomeContractor.purchased}</Text>
              <Text style={styles.sub2month}>{_HomeContractor.valid}</Text>
            </View>
          </ImageBackground>
        </View>

        <View style={styles.rowPlan}>
          <Text style={styles.sub2text}>{_HomeContractor.video}</Text>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text style={styles.sub3text}>{_HomeContractor.showAll}</Text>
            <Image
              style={styles.videoimg}
              source={require('../../images/video.png')}
            />
          </View>
        </View>

        <FlatList
          style={{width: '100%', marginTop: 3}}
          data={listVideoTutorial}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItemVideoTutorial}
        />
      </ScrollView>
    </View>
  );
};
export default HomeContractor;

const styles = StyleSheet.create({
  videoView: {
    width: 140,
    elevation: 2,
    backgroundColor: '#FFF',
    flexDirection: 'column',
    marginHorizontal: 10,
    marginBottom: 40,
    borderRadius: 10,
  },
  subsInternalText: {
    flexDirection: 'row',
    marginTop: 10,
    width: '88%',
    marginHorizontal: 10,
  },
  rowPlan: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    alignSelf: 'center',
  },
  textWhiteavailable: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '900',
    fontSize: 16,
    color: '#FFF',
    marginLeft: 15,
  },
  textWhite: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#FFF',
  },
  imageBackView: {
    marginTop: 10,
    width: '90%',
    alignSelf: 'center',
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginTop: 20,
  },
  container: {
    backgroundColor: '#FFF',
    flex: 1,
  },
  image: {
    width: 40,
    height: 47,
  },
  text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#606E87',
    marginTop: 2,
  },

  search2: {
    marginTop: 5,
    width: 30,
    height: 30,
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    elevation: 5,
  },
  subimage: {
    width: 15,
    height: 15,
    alignSelf: 'center',
    marginTop: 7,
  },

  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  texted: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginTop: 20,
    marginHorizontal: 20,
  },
  contract: {
    width: 160,
    height: 120,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 20,
    marginBottom: 10,
    marginHorizontal: 20,
  },
  contractimg: {
    width: 50,
    height: 50,
    marginTop: 10,
    marginHorizontal: 10,
  },
  contracttext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 15,
    color: '#454545',
    marginTop: 20,
    marginBottom: 20,
  },
  plan: {
    width: 208,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 5,
    marginHorizontal: 10,
    marginBottom: 4,
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
    marginHorizontal: 20,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  tickimg: {
    width: 14,
    height: 14,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    marginTop: -2,
    marginRight: 10,
    marginLeft: 5,
    color: '#8A8A8A',
  },
  touch: {
    width: 100,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 3,
  },
  month: {
    borderRadius: 10,
    width: '100%',
  },
  monthtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
    marginHorizontal: 20,
    marginTop: 15,
  },
  submonth: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 5,
    marginLeft: -15,
  },
  sub2month: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 20,
    marginHorizontal: 20,
    textAlign: 'center',
  },
  video: {
    width: 140,
    height: 100,
    backgroundColor: '#0000004d',
    borderRadius: 15,
  },
  play: {
    marginTop: 37,
    width: 30,
    height: 30,
    alignSelf: 'center',
  },
  playtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 10,
  },
  subplaytext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#3333334d',
    marginTop: 5,
    marginBottom: 20,
  },
});
