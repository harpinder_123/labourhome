import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import stringsoflanguages from '../../language';

const SubscriptionPlanContractor = ({navigation}) => {
  const {_SubscriptionPlanContractor} = stringsoflanguages;
  const [listSubscription, setListSubscription] = useState([
    {
      keys: 5,
    },
    {
      keys: 6,
    },
    {
      keys: 7,
    },
    {
      keys: 8,
    },
  ]);
  const renderItemSubscriptionPlan = ({item, index}) => {
    return (
      <View style={styles.plan}>
        <View
          style={{
            backgroundColor: '#ECEFF1',
            borderTopEndRadius: 10,
            borderTopLeftRadius: 10,
          }}>
          <View
            style={{
              width: '90%',
              alignSelf: 'center',
            }}>
            <Text style={styles.plantext}>
              {_SubscriptionPlanContractor.monthPlan}
            </Text>
            <Text style={styles.subplantext}>₹300</Text>
          </View>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>
            {_SubscriptionPlanContractor.loremTitle}
          </Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>
            {_SubscriptionPlanContractor.loremSubtitle}
          </Text>
        </View>
        <View style={styles.subsInternalText}>
          <Image
            style={styles.tickimg}
            source={require('../../images/tick.png')}
          />
          <Text style={styles.ticktext}>
            {_SubscriptionPlanContractor.loremTitle}
          </Text>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() =>
            navigation.navigate('PaymentContractorSubscriptionPlan')
          }>
          <Text style={styles.touchtext}>
            {_SubscriptionPlanContractor.buy}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_SubscriptionPlanContractor.title}
      />
      <ScrollView>
        <FlatList
          style={{width: '100%', marginTop: 3}}
          data={listSubscription}
          horizontal={false}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItemSubscriptionPlan}
        />
      </ScrollView>
    </View>
  );
};

export default SubscriptionPlanContractor;

const styles = StyleSheet.create({
  subsInternalText: {
    flexDirection: 'row',
    marginTop: 10,
    width: '88%',
    marginHorizontal: 10,
  },
  rowPlan: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    alignSelf: 'center',
  },
  textWhiteavailable: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '900',
    fontSize: 16,
    color: '#FFF',
    marginLeft: 15,
  },
  textWhite: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 18,
    color: '#FFF',
  },
  imageBackView: {
    marginTop: 10,
    width: '90%',
    alignSelf: 'center',
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginTop: 20,
  },
  plan: {
    width: '90%',
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 10,
    marginHorizontal: 10,
    marginBottom: 4,
    alignSelf: 'center',
  },
  plantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
    marginHorizontal: 20,
  },
  subplantext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333333',
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 10,
  },
  tickimg: {
    width: 14,
    height: 14,
  },
  ticktext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    marginTop: -2,
    marginRight: 10,
    marginLeft: 5,
    color: '#8A8A8A',
  },
  touch: {
    width: 100,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 3,
  },
  month: {
    borderRadius: 10,
    width: '100%',
  },
  monthtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffffff',
    marginHorizontal: 20,
    marginTop: 15,
  },
  submonth: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 5,
    marginLeft: -15,
  },
  sub2month: {
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 20,
    marginHorizontal: 20,
    textAlign: 'center',
  },
  video: {
    width: 140,
    height: 100,
    backgroundColor: '#0000004d',
    borderRadius: 15,
  },
  play: {
    marginTop: 37,
    width: 30,
    height: 30,
    alignSelf: 'center',
  },
  playtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 10,
  },
  subplaytext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#3333334d',
    marginTop: 5,
    marginBottom: 20,
  },
});
