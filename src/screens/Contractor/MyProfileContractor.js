import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, Text, View, Image} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import stringsoflanguages from '../../language';

const MyProfileContractor = ({navigation}) => {
  const {_MyProfileContractor} = stringsoflanguages;
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_MyProfileContractor.title}
      />
      <ScrollView>
        <Image
          style={styles.topProfileImage}
          source={require('../../images/pic.png')}
        />
        <Text style={styles.nameText}>{_MyProfileContractor.name}</Text>
        <Text style={styles.changeText}>{_MyProfileContractor.photo}</Text>
        <View style={styles.rowBottomView}>
          <Text style={styles.namingText}>{_MyProfileContractor.Name}</Text>
          <View style={styles.rowValueView}>
            <Text style={styles.namingTextValue}>
              {_MyProfileContractor.name}
            </Text>
          </View>
        </View>
        <View style={styles.rowBottomView}>
          <Text style={styles.namingText}>{_MyProfileContractor.phone}</Text>
          <View style={styles.rowValueView}>
            <Text style={styles.namingTextValue}>+91 9876546382</Text>
          </View>
        </View>
        <View style={styles.rowBottomView}>
          <Text style={styles.namingText}>{_MyProfileContractor.Email}</Text>
          <View style={styles.rowValueView}>
            <Text style={styles.namingTextValue}>rahul@gmail.com</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default MyProfileContractor;

const styles = StyleSheet.create({
  topProfileImage: {
    width: 130,
    height: 130,
    resizeMode: 'contain',
    alignSelf: 'center',
    borderRadius: 75,
    marginTop: 25,
  },
  rowValueView: {
    width: '60%',
  },
  rowBottomView: {
    justifyContent: 'space-between',
    alignSelf: 'center',
    width: '90%',
    flexDirection: 'row',
    marginTop: 20,
  },
  changeText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 12,
    lineHeight: 16,
    color: '#8A94A3',
    marginTop: 3,
    marginHorizontal: 30,
    alignSelf: 'center',
  },
  nameText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    lineHeight: 22,
    color: '#2A3B56',
    marginTop: 10,
    marginHorizontal: 30,
    alignSelf: 'center',
  },
  namingText: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 25,
    color: '#8A94A3',
    textAlign: 'justify',
  },
  namingTextValue: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 25,
    color: '#000000',
    alignSelf: 'flex-end',
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 25,
    color: '#454545',
    textAlign: 'justify',
    marginTop: 10,
    marginHorizontal: 20,
  },
});
