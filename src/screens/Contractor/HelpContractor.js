import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import {TouchableRipple} from 'react-native-paper';
import stringsoflanguages from '../../language';

const HelpContractor = ({navigation}) => {
  const {_HelpContractor} = stringsoflanguages;
  const [state, setState] = useState({
    name: '',
    email: '',
    message: '',
    data: '',
    isLoading: false,
  });
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_HelpContractor.title}
      />
      <ScrollView>
        <Image
          style={styles.topImage}
          source={require('../../images/support1.png')}
        />
        <Text style={styles.subtext}>{_HelpContractor.needHelp}</Text>
        <View style={styles.rowVieww}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              marginLeft: 20,
              marginVertical: 15,
            }}>
            <Image
              style={styles.siseImage}
              source={require('../../images/envelopes.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 10}}>
              <Text style={styles.emailText}>{_HelpContractor.Email}</Text>
              <Text style={styles.emailTextBlack}>support@labourhome.com</Text>
            </View>
          </View>
        </View>
        <View style={styles.rowViewBottom}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              marginLeft: 20,
              marginVertical: 15,
            }}>
            <Image
              style={styles.phoneImage}
              source={require('../../images/phone-call.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 10}}>
              <Text style={styles.emailText}>{_HelpContractor.Phone}</Text>
              <Text style={styles.emailTextBlack}>1800784673</Text>
            </View>
          </View>
        </View>
        <View style={styles.rowViewBottomEnd}>
          <View
            style={{
              flexDirection: 'row',
              width: '90%',
              marginLeft: 20,
              marginTop: 10,
              marginBottom: 4,
            }}>
            <Image
              style={styles.getTouchImage}
              source={require('../../images/help1.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 10}}>
              <Text style={styles.emailTextBlack}>{_HelpContractor.touch}</Text>
            </View>
          </View>
          <Text style={styles.getTouchText}>{_HelpContractor.touchText}</Text>
          <Text style={styles.nameText}>{_HelpContractor.Name}</Text>
          <TextInput
            style={{
              fontFamily: 'Avenir-Heavy',
              fontSize: 16,
              color: '#000000',
              fontWeight: '900',
              marginHorizontal: 10,
              backgroundColor: '#FFF',
            }}
            lineHeight={25}
            placeholder={_HelpContractor.enterName}
            onChangeText={name => setState({...state, name})}
            value={state.name}
          />
          <Text style={styles.textEmail}>{_HelpContractor.Email}</Text>
          <TextInput
            style={{
              fontFamily: 'Avenir-Heavy',
              fontSize: 16,
              color: '#000000',
              marginHorizontal: 10,
              fontWeight: '900',
              backgroundColor: '#FFF',
            }}
            lineHeight={25}
            placeholder={_HelpContractor.Email}
            onChangeText={email => setState({...state, email})}
            value={state.email}
          />

          <Text style={styles.textEmail}>{_HelpContractor.Message}</Text>
          <TextInput
            style={{
              fontFamily: 'Avenir-Heavy',
              fontSize: 16,
              color: '#000000',
              marginHorizontal: 10,
              fontWeight: '900',
              backgroundColor: '#FFF',
            }}
            lineHeight={25}
            placeholder={_HelpContractor.MessageHere}
            onChangeText={message => setState({...state, message})}
            value={state.message}
          />

          <TouchableRipple
            rippleColor="#6CBDFF20"
            style={styles.touch}
            onPress={() => navigation.replace('HomeContractor')}>
            <Text style={styles.touchtext}>{_HelpContractor.SUBMIT}</Text>
          </TouchableRipple>
        </View>
      </ScrollView>
    </View>
  );
};

export default HelpContractor;

const styles = StyleSheet.create({
  touch: {
    padding: 15,
    marginHorizontal: 20,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
  input: {
    color: '#000',
    backgroundColor: 'pink',
    borderBottomColor: '#fff',
    borderBottomWidth: 0,
    tintColor: '#FFF',
  },
  lineView: {
    backgroundColor: '#00000050',
    alignSelf: 'center',
    width: '90%',
    height: 1,
    marginVertical: 10,
  },
  getTouchText: {
    marginTop: 10,
    fontSize: 14,
    lineHeight: 18,
    fontFamily: 'Avenir-Normal',
    fontWeight: '400',
    color: '#7D7D7E',
    marginHorizontal: 20,
    marginBottom: 30,
  },
  emailTextBlack: {
    fontSize: 18,
    lineHeight: 24,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    color: '#000',
  },
  emailText: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    color: '#00000070',
  },
  nameText: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    color: '#00000070',
    marginLeft: 20,
  },
  textEmail: {
    marginTop: 18,
    fontSize: 16,
    lineHeight: 17,
    fontFamily: 'Avenir-Medium',
    fontWeight: '400',
    color: '#00000070',
    marginLeft: 20,
  },
  getTouchImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  phoneImage: {
    width: 28,
    height: 28,
    marginTop: 10,
    resizeMode: 'contain',
  },
  siseImage: {
    width: 28,
    height: 21,
    marginTop: 10,
    resizeMode: 'contain',
  },
  rowVieww: {
    marginTop: 30,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    elevation: 5,
    marginBottom: 2,
    borderRadius: 10,
  },
  rowViewBottom: {
    marginTop: 10,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    elevation: 5,
    marginBottom: 2,
    borderRadius: 10,
  },
  rowViewBottomEnd: {
    marginTop: 10,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    elevation: 5,
    marginBottom: 40,
    borderRadius: 10,
  },
  topImage: {
    width: 150,
    height: 150,
    alignSelf: 'center',
    marginTop: 20,
  },
  subtext: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    lineHeight: 25,
    color: '#000',
    alignSelf: 'center',
    marginTop: 10,
    marginHorizontal: 20,
  },
});
