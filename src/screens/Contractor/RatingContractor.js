import CheckBox from '@react-native-community/checkbox';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {TouchableRipple} from 'react-native-paper';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import stringsoflanguages from '../../language';

export default function RatingContractor({navigation}) {
  const {_RatingContractor} = stringsoflanguages;
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [toggle1CheckBox, setToggle1CheckBox] = useState(false);
  const [toggle2CheckBox, setToggle2CheckBox] = useState(false);
  const [toggle3CheckBox, setToggle3CheckBox] = useState(false);
  const [toggle4CheckBox, setToggle4CheckBox] = useState(false);
  const [toggle5CheckBox, setToggle5CheckBox] = useState(false);
  const [rating, setRating] = useState(2);
  const [maxrating, setmaxRating] = useState([1, 2, 3, 4, 5]);

  const starImgFilled =
    'https://github.com/tranhonghan/images/blob/main/star_filled.png?raw=true';
  const starImgCorner =
    'https://github.com/tranhonghan/images/blob/main/star_corner.png?raw=true';

  const CustomRating = () => {
    return (
      <View style={styles.customRatingStyle}>
        {maxrating.map((item, key) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              key={item}
              onPress={() => setRating(item)}>
              <Image
                style={styles.starImgStyle}
                source={
                  item <= rating ? {uri: starImgFilled} : {uri: starImgCorner}
                }
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  return (
    <View style={styles.topview}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_RatingContractor.title}
      />
      <ScrollView>
        <View style={styles.container}>
          <Image
            style={styles.image}
            source={require('../../images/pic.png')}
          />
          <View style={styles.rowdataManage}>
            <Text style={styles.containerText}>{_RatingContractor.name}</Text>
            <Text style={styles.containersubtext}>
              {_RatingContractor.Plumber}
            </Text>
          </View>
        </View>
        <Text style={styles.text}>{_RatingContractor.rate}</Text>
        <Text style={styles.subtext}>{_RatingContractor.subRate}</Text>
        <CustomRating />
        <Text style={styles.sub2text}>{_RatingContractor.perfect}</Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'AVenir-Medium',
              fontWeight: '500',
              color: '#000000',
            }}>
            {_RatingContractor.price}
          </Text>
          <CheckBox
            disabled={false}
            value={toggleCheckBox}
            onValueChange={newValue => setToggleCheckBox(newValue)}
            tintColors={{
              true: '#F2AD4B',
              false: 'grey',
            }}
          />
        </View>
        <View style={styles.Line} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'AVenir-Medium',
              fontWeight: '500',
              color: '#000000',
            }}>
            {_RatingContractor.quality}
          </Text>
          <CheckBox
            disabled={false}
            value={toggle1CheckBox}
            onValueChange={newValue => setToggle1CheckBox(newValue)}
            tintColors={{
              true: '#F2AD4B',
              false: 'grey',
            }}
          />
        </View>
        <View style={styles.Line} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'AVenir-Medium',
              fontWeight: '500',
              color: '#000000',
            }}>
            {_RatingContractor.Behavior}
          </Text>
          <CheckBox
            disabled={false}
            value={toggle2CheckBox}
            onValueChange={newValue => setToggle2CheckBox(newValue)}
            tintColors={{
              true: '#F2AD4B',
              false: 'grey',
            }}
          />
        </View>
        <View style={styles.Line} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'AVenir-Medium',
              fontWeight: '500',
              color: '#000000',
            }}>
            {_RatingContractor.Cleanliness}
          </Text>
          <CheckBox
            disabled={false}
            value={toggle3CheckBox}
            onValueChange={newValue => setToggle3CheckBox(newValue)}
            tintColors={{
              true: '#F2AD4B',
              false: 'grey',
            }}
          />
        </View>
        <View style={styles.Line} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'AVenir-Medium',
              fontWeight: '500',
              color: '#000000',
            }}>
            {_RatingContractor.Punctuality}
          </Text>
          <CheckBox
            disabled={false}
            value={toggle4CheckBox}
            onValueChange={newValue => setToggle4CheckBox(newValue)}
            tintColors={{
              true: '#F2AD4B',
              false: 'grey',
            }}
          />
        </View>
        <View style={styles.Line} />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'AVenir-Medium',
              fontWeight: '500',
              color: '#000000',
            }}>
            {_RatingContractor.Skills}
          </Text>
          <CheckBox
            disabled={false}
            value={toggle5CheckBox}
            onValueChange={newValue => setToggle5CheckBox(newValue)}
            tintColors={{
              true: '#F2AD4B',
              false: 'grey',
            }}
          />
        </View>
        <Text style={styles.feedback}>{_RatingContractor.feedback}</Text>
        <View style={styles.boxes}>
          <TextInput style={styles.textinput} multiline={true} placeholder="" />
        </View>
        <TouchableRipple
          rippleColor="#6CBDFF20"
          style={styles.touch}
          onPress={() => navigation.replace('HomeContractor')}>
          <Text style={styles.touchtext}>{_RatingContractor.SUBMIT}</Text>
        </TouchableRipple>
      </ScrollView>
    </View>
  );
}
const styles = StyleSheet.create({
  rowdataManage: {
    width: '70%',
    alignSelf: 'center',
    marginBottom: 10,
  },
  topview: {
    backgroundColor: '#fff',
    flex: 1,
  },
  container: {
    backgroundColor: '#F2AD4B',
  },
  containerText: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
    marginTop: 10,
  },
  containersubtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    marginTop: 4,
    textAlign: 'center',
    color: '#fff',
    marginBottom: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    marginTop: 5,
    textAlign: 'center',
    color: '#3333334d',
  },
  sub2text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 15,
    color: '#000000',
    marginHorizontal: 20,
  },
  subview: {
    backgroundColor: '#b9dcf1',
    height: 150,
    width: 360,
    marginTop: 120,
    marginHorizontal: 15,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    flexDirection: 'row',
  },
  image: {
    height: 80,
    width: 80,
    borderRadius: 40,
    marginTop: 28,
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#000000',
    marginTop: 20,
  },
  subtxt: {
    color: 'grey',
    fontWeight: 'bold',
    marginLeft: -10,
  },
  customRatingStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 5,
    // marginLeft: -40,
  },
  starImgStyle: {
    height: 25,
    width: 25,
    margin: 5,
    resizeMode: 'cover',
  },
  txted: {
    marginHorizontal: -90,
    marginTop: 20,
    borderWidth: 0.5,
    borderColor: 'lightgrey',
    borderRadius: 10,
    height: 310,
    marginLeft: -110,
  },
  btn: {
    marginTop: 400,
    backgroundColor: 'dodgerblue',
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
    marginHorizontal: 20,
  },
  btntxt: {
    color: 'white',
    marginHorizontal: 20,
    fontWeight: 'bold',
    fontSize: 18,
  },
  Line: {
    height: 1,
    backgroundColor: '#dadce0aa',
    marginTop: 5,
    marginHorizontal: 20,
    // marginLeft: 25,
  },
  feedback: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000',
    marginTop: 20,
    marginHorizontal: 20,
  },
  textinput: {
    marginTop: 5,
    marginHorizontal: 10,
  },
  boxes: {
    height: 100,
    width: '90%',
    borderRadius: 20,
    borderWidth: 1,
    alignSelf: 'center',
    borderColor: '#BFC4D1',
    backgroundColor: '#fff',
    marginTop: 10,
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 30,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
