import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Modal,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {Header} from '../../Custom/CustomView';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {RadioButton} from 'react-native-paper';
const {height, width} = Dimensions.get('window');

let data = [
  {
    value: 'I am a worker',
  },
  {
    value: 'I am a contractor',
  },
  {
    value: 'I am a customer',
  },
];

const ReviewJobContractor = ({navigation}) => {
  const [checked, setChecked] = useState('first');
  const [tick, setTick] = useState('third');
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={'Review Job Post'} />
      <ScrollView>
        <View style={{width: '66%', alignSelf: 'center'}}>
          <Text style={styles.textMain}>
            Now just finish and review your job post
          </Text>
        </View>
        <Text style={styles.text}>Job Heading</Text>
        <View style={styles.boxes}>
          <TextInput
            style={styles.textinput}
            editable={false}
            placeholder="XYZ Jobs"
          />
        </View>

        <Text style={styles.text}>Job Heading</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s
          </Text>
        </View>
        <Text style={styles.text}>12/12/2021</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>12/12/2021</Text>
        </View>
        <Text style={styles.text}>How long will your work take?</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>Longer term work</Text>
        </View>
        <Text style={styles.text}>Job Location</Text>
        <View style={styles.home}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 10}}>
              <RadioButton
                value="fourth"
                status={tick === 'fourth' ? 'checked' : 'unchecked'}
                onPress={() => setTick('fourth')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.hometext}>Work</Text>
          </View>
          <Text style={styles.home2text}>
            21/C9, 2nd Floor, Sector-7, Rohini Opp Metro Pillor No. 400, New
            Delhi - 110085 (IN)
          </Text>
        </View>
        <Text style={styles.text}>Project Type</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>Interior Designing</Text>
        </View>
        <Text style={styles.text}>Area in sq ft</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>500</Text>
        </View>
        <Text style={styles.text}>Specification</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry.
          </Text>
        </View>
        <Text style={styles.text}>Budget</Text>
        <View style={styles.boxing}>
          <Text style={styles.sub4text}>₹10000</Text>
        </View>

        <Text style={styles.text}>Site Images/Videos</Text>
        <View style={styles.rowSecondData}>
          <Image
            style={{
              width: 90,
              height: 80,
              resizeMode: 'contain',
              borderRadius: 10,
            }}
            source={require('../../images/Group2.png')}
          />
          <ImageBackground
            imageStyle={styles.bgImageStyle}
            source={require('../../images/Group2.png')}>
            <Image
              style={{height: 35, width: 35, marginLeft: 43, marginTop: 23}}
              source={require('../../images/playbtn.png')}
            />
          </ImageBackground>
        </View>

        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('PostJobContractor')}>
          <Text style={styles.touchtext}>Post Job Now</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default ReviewJobContractor;

const styles = StyleSheet.create({
  bgImageStyle: {
    width: 90,
    height: 80,
    marginLeft: 15,
    resizeMode: 'contain',
    borderRadius: 10,
  },
  rowSecondData: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 10,
  },
  textLeft: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    fontWeight: '800',
    lineHeight: 18,
    marginLeft: 15,
  },
  sub4text: {
    fontFamily: 'Avenir-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#454545',
    marginHorizontal: 15,
    marginVertical: 14,
  },
  boxing: {
    width: '90%',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginTop: 10,
    alignSelf: 'center',
  },

  jobHeadingText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#454545',
    textAlign: 'justify',
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    marginHorizontal: 20,
    marginTop: 16,
    color: '#454545',
  },
  textMain: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 20,
    alignSelf: 'center',
    textAlign: 'center',
    marginTop: 16,
    color: '#454545',
  },
  hometext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 10,
    color: '#454545',
  },

  textinput: {
    color: '#000',
    marginHorizontal: 10,
  },
  boxes: {
    height: 50,
    width: '90%',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginTop: 10,
    alignSelf: 'center',
  },

  home: {
    padding: 10,
    marginHorizontal: 15,
    backgroundColor: '#fff',
    borderRadius: 15,
    elevation: 2,
    marginTop: 15,
  },
  home2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: 11,
    fontWeight: '500',
    marginHorizontal: 45,
    lineHeight: 20,
    marginTop: -20,
  },

  touch: {
    padding: 15,
    marginHorizontal: 20,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginVertical: 20,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
