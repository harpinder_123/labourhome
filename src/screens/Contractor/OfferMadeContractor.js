import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  FlatList,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../../Custom/CustomStatusBar';
import {Header} from '../../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import {TouchableRipple} from 'react-native-paper';
import stringsoflanguages from '../../language';

const OfferMadeContractor = ({navigation}) => {
  const {_OfferMadeContractor} = stringsoflanguages;
  const [listdata, setListdata] = useState([
    {
      keys: 5,
    },
    {
      keys: 6,
    },
    {
      keys: 7,
    },
    {
      keys: 8,
    },
  ]);
  const onPressCategory = ({item, index}) => {
    navigation.navigate('OfferMadeContractorDetails');
  };

  const renderItemOffersMade = ({item, index}) => {
    return (
      <TouchableRipple
        style={{
          flexDirection: 'row',
          width: '90%',
          alignSelf: 'center',
          marginVertical: 10,
          justifyContent: 'space-between',
        }}
        onPress={() => onPressCategory(item, index)}>
        <View style={styles.container}>
          <View style={styles.jobView}>
            <Text style={styles.text}>{_OfferMadeContractor.xyzJob}</Text>
          </View>
          <Text style={styles.timeTextt}>{_OfferMadeContractor.loremtext}</Text>

          <DashLine />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
              alignSelf: 'center',
            }}>
            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_OfferMadeContractor.startDate}</Text>
              <Text style={styles.subDate}>12/12/2021</Text>
            </View>

            <View style={{width: '30%'}}>
              <Text style={styles.date}>{_OfferMadeContractor.duration}</Text>
              <Text style={styles.subDate}>
                {_OfferMadeContractor.shortTerm}
              </Text>
            </View>

            <View style={{width: '25%'}}>
              <Text style={styles.date}>{_OfferMadeContractor.budget}</Text>
              <Text style={styles.subDate}>₹ 5000</Text>
            </View>
          </View>
          <DashLine />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'center',
              width: '90%',
            }}>
            <View style={{width: '48%'}}>
              <Text style={styles.date}>{_OfferMadeContractor.project}</Text>
              <Text style={styles.subDate}>
                {_OfferMadeContractor.interior}
              </Text>
            </View>

            <View style={{width: '48%'}}>
              <Text style={styles.date}>
                {_OfferMadeContractor.jobLocation}
              </Text>
              <Text style={styles.subDate}>{_OfferMadeContractor.address}</Text>
            </View>
          </View>
        </View>
      </TouchableRipple>
    );
  };
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_OfferMadeContractor.title}
      />
      <FlatList
        style={{width: '100%', marginTop: 3}}
        data={listdata}
        showsVerticalScrollIndicator={false}
        renderItem={renderItemOffersMade}
      />
    </View>
  );
};

export default OfferMadeContractor;

const styles = StyleSheet.create({
  timeTextt: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginLeft: 12,
    marginTop: 5,
  },
  timeText: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    marginRight: 5,
    color: '#8A94A3',
    marginTop: 8,
  },
  jobView: {
    justifyContent: 'center',
    width: '90%',
    marginLeft: 12,
  },
  textdays: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
    marginHorizontal: 10,
  },
  redDaysView: {
    justifyContent: 'center',
    backgroundColor: '#E02020',
    borderRadius: 10,
    height: 36,
  },
  textShortBlack: {
    alignSelf: 'center',
    color: '#6D7278',
    marginVertical: 7,
  },
  textViewDetailsWhite: {
    alignSelf: 'center',
    color: '#FFF',
    marginVertical: 7,
  },
  rowDataButtonBlue: {
    justifyContent: 'center',
    width: '47%',
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
  },
  rowDataButton: {
    justifyContent: 'center',
    width: '47%',
    borderColor: '#6D7278',
    borderWidth: 1,
    borderRadius: 20,
  },
  container: {
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 8,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 15,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
  },
  subText: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginHorizontal: 10,
    marginTop: 5,
    color: '#454545',
  },
  date: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
  },
  subDate: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#454545',
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 10,
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 50,
    marginTop: 20,
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
