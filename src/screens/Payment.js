import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  Modal,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import {RadioButton} from 'react-native-paper';
import stringsoflanguages from '../language';
const {height} = Dimensions.get('window');

const Payment = ({navigation}) => {
  const {_PaymentContractor, _success} = stringsoflanguages;
  const [modalOpen, setModalOpen] = useState(false);
  const [checked, setChecked] = useState('first');
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header
        onPress={() => navigation.goBack()}
        title={_PaymentContractor.title}
      />
      <ScrollView>
        <View style={styles.container}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.text}>{_PaymentContractor.amount}</Text>
              <Text style={styles.subtext}>{_PaymentContractor.taxes}</Text>
            </View>
            <Text style={styles.sub2text}>₹5000</Text>
          </View>
        </View>
        <View style={styles.subContainer}>
          <Text style={styles.sub3text}>{_PaymentContractor.payment}</Text>
        </View>

        <View style={styles.topv4_Style}>
          <View>
            <Text style={styles.text2_Style}>{_PaymentContractor.debit}</Text>
          </View>
          <RadioButton
            value="first"
            status={checked === 'first' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('first')}
            uncheckedColor={'#69707F'}
            color={'#7BAAED'}
          />
        </View>

        <View style={styles.topv4_Style}>
          <View>
            <Text style={styles.text2_Style}>{_PaymentContractor.credit}</Text>
          </View>
          <RadioButton
            value="second"
            status={checked === 'second' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('second')}
            uncheckedColor={'#69707F'}
            color={'#7BAAED'}
          />
        </View>

        <View style={styles.topv4_Style}>
          <View>
            <Text style={styles.text2_Style}>
              {_PaymentContractor.netBanking}
            </Text>
          </View>
          <RadioButton
            value="third"
            status={checked === 'third' ? 'checked' : 'unchecked'}
            onPress={() => setChecked('third')}
            uncheckedColor={'#69707F'}
            color={'#7BAAED'}
          />
        </View>

        <TouchableOpacity
          style={styles.touch}
          onPress={() => setModalOpen(true)}>
          <Text style={styles.touchtext}>{_PaymentContractor.payNow}</Text>
        </TouchableOpacity>
        <Modal
          visible={modalOpen}
          transparent={true}
          onRequestClose={() => setModalOpen(false)}>
          <View style={styles.modal_View}>
            <View style={styles.mdtop}>
              {/* <View style={styles.mdtop_1}>
            <Text style={styles.mdTopText}>Assign Driver:</Text>
          </View> */}
              <Image
                style={{
                  width: 70,
                  height: 70,
                  alignSelf: 'center',
                  marginTop: 20,
                }}
                source={require('../images/mark.png')}
              />
              <Text style={styles.paytext}>{_success.payment}</Text>

              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.mdbottomView}
                onPress={() => navigation.navigate('PostJob')}>
                <Text style={styles.mdBottomText}>{_success.post}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </ScrollView>
    </View>
  );
};

export default Payment;

const styles = StyleSheet.create({
  plan: {
    width: 340,
    height: 270,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 5,
    marginTop: 20,
    marginBottom: 10,
    marginHorizontal: 25,
  },
  container: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#F2AD4B',
    borderRadius: 10,
    marginTop: 20,
  },
  text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff',
    marginHorizontal: 10,
  },
  subtext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#ffffff',
    marginHorizontal: 10,
  },
  sub2text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff',
    marginHorizontal: 10,
    marginTop: 10,
  },
  subContainer: {
    width: '100%',
    padding: 10,
    backgroundColor: '#fff',
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#263238',
    marginHorizontal: 20,
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    color: '#263238',
    fontWeight: 'bold',
    marginHorizontal: 10,
  },
  subtext2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: '80%',
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  paytext: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 22,
    color: '#333333',
    textAlign: 'center',
    marginTop: 10,
  },
  mdbottomView: {
    backgroundColor: '#6CBDFF',
    borderRadius: 20,
    padding: 10,
    marginTop: 40,
    // marginHorizontal: 25,
    width: 160,
    height: 40,
    marginBottom: 20,
    alignSelf: 'center',
  },
  mdBottomText: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  topv4_Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  text2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 20,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  subtext2_Style: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    color: '#9F9F9F',
    fontWeight: 'bold',
  },
  line: {
    borderColor: '#00000020',
    borderWidth: 0.5,
    marginTop: 15,
  },
});
