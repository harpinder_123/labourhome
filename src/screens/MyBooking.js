import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header} from '../Custom/CustomView';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Dash from 'react-native-dash';
import {useNavigation} from '@react-navigation/native';
import stringsoflanguages from '../language';

const Ongoing = () => {
  const {_myBooking} = stringsoflanguages;
  return (
    <ScrollView>
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>{_myBooking.booking}</Text>
              <Text style={styles.subBooking}>{_myBooking.date}</Text>
            </View>
            <Image
              style={{
                width: 25,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: 20,
                marginTop: 5,
              }}
              source={require('../images/phone.png')}
            />
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.imagebox}
                  source={require('../images/photo.png')}
                />
                <Text style={styles.inputtxt}>{_myBooking.name}</Text>
              </View>
              <Text style={styles.subtext}>{_myBooking.plumber}</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>
            <Image
              style={{
                width: 25,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: -50,
              }}
              source={require('../images/share.png')}
            />
            <Text style={styles.location}>{_myBooking.location}</Text>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>{_myBooking.otp}</Text>
              <Text style={styles.subBooking}>{_myBooking.otpTitle}</Text>
            </View>
            <Text style={styles.otptext}>3456</Text>
          </View>
        </View>

        <View
          style={{
            padding: 10,
            marginHorizontal: 20,
            backgroundColor: '#fff',
            elevation: 5,
            marginTop: 20,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>{_myBooking.booking}</Text>
              <Text style={styles.subBooking}>{_myBooking.date}</Text>
            </View>
            <Image
              style={{
                width: 25,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: 20,
                marginTop: 5,
              }}
              source={require('../images/phone.png')}
            />
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={styles.imagebox}
                  source={require('../images/photo.png')}
                />
                <Text style={styles.inputtxt}>{_myBooking.name}</Text>
              </View>
              <Text style={styles.subtext}>{_myBooking.plumber}</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>
            <Image
              style={{
                width: 25,
                height: 25,
                marginLeft: 'auto',
                marginHorizontal: -50,
              }}
              source={require('../images/share.png')}
            />
            <Text style={styles.location}>{_myBooking.location}</Text>
          </View>
          <DashLine />
          <View style={{flexDirection: 'row'}}>
            <View>
              <Text style={styles.booking}>{_myBooking.otp}</Text>
              <Text style={styles.subBooking}>{_myBooking.otpTitle}</Text>
            </View>
            <Text style={styles.otptext}>3456</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const Completed = () => {
  const {_myBooking} = stringsoflanguages;
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          padding: 10,
          marginHorizontal: 20,
          backgroundColor: '#fff',
          elevation: 5,
          marginTop: 20,
          borderRadius: 10,
        }}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.booking}>{_myBooking.booking}</Text>
            <Text style={styles.subBooking}>{_myBooking.date}</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.imagebox}
            source={require('../images/photo.png')}
          />
          <Text style={styles.inputtxt}>{_myBooking.name}</Text>
        </View>
        <Text style={styles.subtext}>{_myBooking.plumber}</Text>
        <Image
          style={styles.sub3image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub4image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub5image}
          source={require('../images/star.png')}
        />
        <DashLine />
        <TouchableOpacity onPress={() => navigation.navigate('RatingHome')}>
          <Text style={styles.ratetext}>{_myBooking.rate}</Text>
        </TouchableOpacity>
      </View>

      <View
        style={{
          padding: 10,
          marginHorizontal: 20,
          backgroundColor: '#fff',
          elevation: 5,
          marginTop: 20,
          borderRadius: 10,
        }}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.booking}>{_myBooking.booking}</Text>
            <Text style={styles.subBooking}>{_myBooking.date}</Text>
          </View>
        </View>
        <DashLine />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.imagebox}
            source={require('../images/photo.png')}
          />
          <Text style={styles.inputtxt}>{_myBooking.name}</Text>
        </View>
        <Text style={styles.subtext}>{_myBooking.plumber}</Text>
        <Image
          style={styles.sub3image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub4image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub5image}
          source={require('../images/star.png')}
        />
        <DashLine />
        <Text style={styles.subratetext}>{_myBooking.rating}</Text>
      </View>
    </View>
  );
};

const renderScene = SceneMap({
  first: Ongoing,
  second: Completed,
});

const MyBooking = () => {
  const {_myBooking} = stringsoflanguages;
  const navigation = useNavigation();
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: _myBooking.ongoing},
    {key: 'second', title: _myBooking.completed},
  ]);
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={_myBooking.title} />
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        renderTabBar={props => (
          <TabBar
            style={styles.style}
            labelStyle={styles.labelStyle}
            tabStyle={{}}
            scrollEnabled={false}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            inactiveOpacity={0.5}
            {...props}
            indicatorStyle={styles.indicatorStyle}
          />
        )}
      />
    </View>
  );
};

export default MyBooking;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: '100%',
    backgroundColor: '#F2AD4B',
    marginTop: 10,
  },
  style: {backgroundColor: '#F2AD4B'},
  labelStyle: {
    fontSize: 14,
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
  },
  indicatorStyle: {backgroundColor: '#fff', height: 3},
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    marginHorizontal: 20,
  },
  imagebox: {
    // marginTop: 40,
    // marginHorizontal: 20,
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  inputtxt: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#080040',
    marginHorizontal: 10,
  },
  sub3image: {
    marginHorizontal: 60,
    marginTop: 20,
    height: 14,
    width: 14,
    marginTop: 5,
  },
  sub4image: {
    marginHorizontal: 75,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub5image: {
    marginHorizontal: 90,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  subtext: {
    fontFamily: 'Avenir-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
    marginHorizontal: 60,
    marginTop: -30,
  },
  touch: {
    width: 150,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6DD400',
    marginTop: 20,
    // marginBottom: 10,
    // alignSelf: 'center',
  },
  touch2: {
    width: 150,
    height: 35,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 10,
    // alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
  booking: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#454545',
  },
  subBooking: {
    fontFamily: 'Avenir-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#A5A5A5',
    marginTop: 5,
  },
  location: {
    fontFamily: 'Avenir-Medium',
    fontSize: 10,
    fontWeight: '500',
    color: '#333333',
    marginTop: 30,
    marginHorizontal: 15,
    textAlign: 'center',
  },
  otptext: {
    fontFamily: 'Avenir',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 60,
  },
  ratetext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#F2AD4B',
    textAlign: 'center',
  },
  subratetext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
  },
});
const DashLine = props => (
  <View style={{margin: 10}}>
    <Dash dashColor={'#C8C8D3'} dashLength={5} dashThickness={1} {...props} />
  </View>
);
