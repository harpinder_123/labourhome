import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import store from '../redux/store';

const DailyJob = () => {
  const {customerHomeJob} = useSelector(store => store);
  const listDataSix = customerHomeJob.length;
  const navigation = useNavigation();

  let colors = [
    '#FF8A804d',
    '#EDE7F6',
    '#E1F5FE',
    '#F3E5F5',
    '#FFF8E1',
    '#FBE9E7',
    '#FCE4EC',
    '#E8EAF6',
  ];
  useEffect(() => {
    /// alert(JSON.stringify(listDataSix, null, 2));
  }, []);
  return (
    <View style={styles.container}>
      <ScrollView>
        <FlatList
          style={{
            width: '90%',
            alignSelf: 'center',
          }}
          numColumns={3}
          keyExtractor={item => item.id}
          data={customerHomeJob}
          renderItem={({item, index}) => (
            <TouchableOpacity
              style={[
                styles.touch,
                {backgroundColor: colors[index % colors.length]},
              ]}
              onPress={() => navigation.navigate('CallList', {id: item.id})}>
              <Image source={{uri: item.image_url}} style={styles.images} />
              <Text style={styles.title}>{item.name}</Text>
            </TouchableOpacity>
          )}
        />
      </ScrollView>
    </View>
  );
};
export default DailyJob;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F4F4F4',
    flex: 1,
  },
  sub2text: {
    fontFamily: 'Avenir',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#454545',
    marginBottom: 20,
    marginHorizontal: 20,
    marginTop: 20,
  },
  sub3text: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#6CBDFF',
    marginTop: 20,
  },
  videoimg: {
    width: 6,
    height: 10,
    marginLeft: 5,
    marginTop: 25,
  },
  touch: {
    width: '30.4%',
    height: 90,
    borderRadius: 10,
    marginVertical: 10,
    marginHorizontal: 5,
  },
  images: {
    width: 35,
    height: 35,
    alignSelf: 'center',
    marginTop: 15,
  },
  title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 10,
    color: '#454545',
    textAlign: 'center',
    marginTop: 8,
  },
});
