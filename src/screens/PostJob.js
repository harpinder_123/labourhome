import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Modal,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {Header} from '../Custom/CustomView';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {RadioButton} from 'react-native-paper';
import stringsoflanguages from '../language';
const {height, width} = Dimensions.get('window');

const PostJob = ({navigation}) => {
  const {_postJob} = stringsoflanguages;
  const [checked, setChecked] = useState('first');
  const [tick, setTick] = useState('third');
  const [selectJob, setSelectJob] = useState([
    {value: _postJob.IamWorker},
    {value: _postJob.IamContractor},
    {value: _postJob.IamCustomer},
  ]);
  return (
    <View style={{backgroundColor: '#F4F4F4', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} title={_postJob.title} />
      <ScrollView>
        <Text style={styles.text}>{_postJob.jobTitle}</Text>
        <View style={styles.box}>
          <Text>{_postJob.jobHeading}</Text>
        </View>
        <Text style={styles.text}>{_postJob.templete}</Text>
        <Dropdown
          style={styles.drops}
          underlineColor="transparent"
          label={_postJob.selectType}
          iconColor="#0000004d"
          icon={require('../images/drop.png')}
          data={selectJob}
        />
        <View style={styles.boxes}>
          <TextInput
            style={styles.textinput}
            placeholder={_postJob.description}
          />
        </View>
        <View style={styles.box}>
          <View style={{flexDirection: 'row'}}>
            <Text>{_postJob.jobDate}</Text>
            <Image
              style={{width: 20, height: 20, marginLeft: 'auto'}}
              source={require('../images/calendar.png')}
            />
          </View>
        </View>
        <Text style={styles.text}>{_postJob.work}</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <View style={styles.boxsd}>
            <View style={{alignSelf: 'center', marginTop: 5}}>
              <RadioButton
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.subtext}>{_postJob.shortWork}</Text>
            <Text style={styles.sub2text}>{_postJob.lessWeak}</Text>
            <Text style={styles.sub3text}>{_postJob.lessMonth}</Text>
          </View>

          <View style={styles.boxsd}>
            <View style={{alignSelf: 'center', marginTop: 5}}>
              <RadioButton
                value="second"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('second')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.subtext}>{_postJob.longWork}</Text>
            <Text style={styles.sub2text}>{_postJob.moreWeak}</Text>
            <Text style={styles.sub3text}>{_postJob.month}</Text>
          </View>
        </View>
        <Text style={styles.text}>{_postJob.jobLocation}</Text>
        <TouchableOpacity style={styles.touchButton}>
          <Text style={styles.touchText}>{_postJob.addNew}</Text>
        </TouchableOpacity>
        <View style={styles.home}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 10}}>
              <RadioButton
                value="third"
                status={tick === 'third' ? 'checked' : 'unchecked'}
                onPress={() => setTick('third')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.hometext}>{_postJob.Home}</Text>
          </View>
          <Text style={styles.home2text}>{_postJob.homeText}</Text>
        </View>

        <View style={styles.home}>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginTop: 10}}>
              <RadioButton
                value="fourth"
                status={tick === 'fourth' ? 'checked' : 'unchecked'}
                onPress={() => setTick('fourth')}
                uncheckedColor={'#69707F'}
                color={'#7BAAED'}
              />
            </View>
            <Text style={styles.hometext}>{_postJob.work}</Text>
          </View>
          <Text style={styles.home2text}>{_postJob.homeText}</Text>
        </View>

        <View style={styles.box}>
          <TouchableOpacity style={{flexDirection: 'row'}}>
            <Text>{_postJob.projectType}</Text>
            <Image
              style={{width: 10, height: 16, marginLeft: 'auto'}}
              source={require('../images/arrow.png')}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.text}>{_postJob.budget}</Text>
        <Text style={styles.sub4text}>{_postJob.range}</Text>
        <View style={styles.box}>
          <Text>{_postJob.area}</Text>
        </View>

        <View style={styles.boxes}>
          <TextInput
            style={styles.textinput}
            placeholder={_postJob.specification}
          />
        </View>
        <View style={styles.box}>
          <Text>{_postJob.cost}</Text>
        </View>
        <Text style={styles.text}>{_postJob.upload}</Text>
        <Image
          style={styles.imageBox}
          source={require('../images/choose-file.png')}
        />

        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('Success2')}>
          <Text style={styles.touchtext}>{_postJob.review}</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default PostJob;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 30,
    marginTop: 20,
    color: '#454545',
  },
  hometext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginHorizontal: 10,
    color: '#454545',
  },
  box: {
    padding: 20,
    marginHorizontal: 30,
    backgroundColor: '#fff',
    borderRadius: 15,
    elevation: 5,
    marginTop: 20,
    height: 60,
  },
  drops: {
    height: 60,
    backgroundColor: '#fff',
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0,
    elevation: 2,
  },
  textinput: {
    marginTop: 10,
    marginHorizontal: 10,
  },
  boxes: {
    height: 150,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginTop: 20,
    marginHorizontal: 30,
  },
  boxsd: {
    height: 110,
    width: 160,
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 2,
    marginTop: 20,
    marginHorizontal: 30,
  },
  subtext: {
    textAlign: 'center',
    fontFamily: 'Avenir-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#454545',
  },
  sub2text: {
    textAlign: 'center',
    fontFamily: 'Avenir-Medium',
    fontSize: 9,
    fontWeight: '500',
    color: '#4545454d',
    marginTop: 5,
  },
  sub3text: {
    textAlign: 'center',
    fontFamily: 'Avenir-Medium',
    fontSize: 9,
    fontWeight: '500',
    color: '#4545454d',
  },
  touchButton: {
    width: 90,
    height: 30,
    backgroundColor: '#F2AD4B',
    borderRadius: 8,
    marginTop: -20,
    marginLeft: 'auto',
    marginHorizontal: 20,
  },
  touchText: {
    color: '#fff',
    textAlign: 'center',
    marginTop: 5,
    fontWeight: 'bold',
  },
  home: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#fff',
    borderRadius: 15,
    elevation: 2,
    marginTop: 20,
  },
  home2text: {
    fontFamily: 'Avenir-Medium',
    fontSize: 11,
    fontWeight: '500',
    marginHorizontal: 45,
    lineHeight: 20,
    marginTop: -20,
  },
  sub4text: {
    fontFamily: 'Avenir-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#454545',
    marginHorizontal: 30,
    marginTop: 5,
  },
  imageBox: {
    width: 162,
    height: 115,
    borderRadius: 5,
    backgroundColor: '#fff',
    marginTop: 20,
    marginHorizontal: 30,
  },
  touch: {
    padding: 15,
    marginHorizontal: 30,
    borderRadius: 25,
    backgroundColor: '#6CBDFF',
    marginTop: 20,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
